UNAME := $(shell uname)
$(info OS: $(UNAME))

MCAST_I_PATH=${PWD}/libmcast/build/local/include
MCAST_L_PATH=${PWD}/libmcast/build/local/lib


JNI_PATH=${JAVA_HOME}/include

SOURCES_DIR=./src/main/native
OBJECTS_DIR=./target/classes
SOURCES=$(wildcard $(SOURCES_DIR)/*.c)

ifeq ($(UNAME), Linux)
	CC=gcc
	C_I_PATH="/home/long/.local/include"
	C_L_PATH="/home/long/.local/lib"
	JNI_PATH_D="${JAVA_HOME}/include/linux"
	OBJECTS=$(SOURCES:$(SOURCES_DIR)/%.c=$(OBJECTS_DIR)/lib%.so)
	LDFLAGS=-fPIC -std=c11 -shared -I ${JNI_PATH_D} -I ${JNI_PATH} -I ${MCAST_I_PATH} -I ${C_I_PATH} -levent -levpaxos -lpthread -levent_pthreads -levmcast -levamcast -L ${C_L_PATH} -L ${MCAST_L_PATH} -Wno-deprecated
endif
ifeq ($(UNAME), Darwin) #on macOs
	CC=gcc-4.9	
	C_I_PATH="/usr/local/include"
	C_L_PATH="/usr/local/lib"
	LIBPAXOS_I_PATH="/Users/longle/.local/include"
	LIBPAXOS_L_PATH="/Users/longle/.local/lib"
	JNI_PATH_D="${JAVA_HOME}/include/darwin"
	OBJECTS=$(SOURCES:$(SOURCES_DIR)/%.c=$(OBJECTS_DIR)/lib%.dylib)
	LDFLAGS=-std=c11 -shared -I ${JNI_PATH_D} -I ${JNI_PATH} -I ${MCAST_I_PATH} -I ${C_I_PATH} -I ${LIBPAXOS_I_PATH} -L ${LIBPAXOS_L_PATH} -levent -lpthread -levpaxos -levent_pthreads -levmcast -levamcast -L ${C_L_PATH} -L ${MCAST_L_PATH} -Wno-deprecated
endif


EXECUTABLE=$(OBJECTS_DIR)/libmcast

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

$(OBJECTS_DIR)/lib%.dylib : $(SOURCES_DIR)/%.c
	mkdir -p $(OBJECTS_DIR)
	$(CC) $(LDFLAGS) $< -o $@

$(OBJECTS_DIR)/lib%.so : $(SOURCES_DIR)/%.c
	mkdir -p $(OBJECTS_DIR)
	$(CC) $(LDFLAGS) $< -o $@

clean:
	rm $(OBJECTS) $(EXECUTABLE)

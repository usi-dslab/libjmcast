package ch.usi.dslab.lel.jmcast.jni;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.NodeCallbackHandler;
import ch.usi.dslab.lel.jmcast.Util;
import org.slf4j.LoggerFactory;

/**
 * Created by longle on 19.10.16.
 */
public class McastNodeJNI extends Thread {
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(McastNodeJNI.class);

    public native void jni_init(int groupId, int nodeId, String mconfig, String pconfig);

    public native void jni_reply_to_client(int clientId, String message);

    public native int jni_is_connected_to_client(int clientId);

    public native void jni_start();

    NodeCallbackHandler cbOnDeliver;

    static {
        try {
            Util.loadLibrary("McastNode");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public boolean isConnectedToClient(int clientId) {
        if (this.jni_is_connected_to_client(clientId) == 0) return false;
        return true;
    }

    public void deliver(int clientId, String message) {
        logger.debug("ON_DELIVER {} of client {}", message, clientId);
//        System.out.println("JAVA receive " + message);
        reply(clientId, new Message(1));
//        cbOnDeliver.onDeliver(clientId, Message.deserializeFromString(message));
    }

    private void reply(int clientId, String message) {
        if (this.isConnectedToClient(clientId)) this.jni_reply_to_client(clientId, message);
    }

    public void reply(int clientId, Message message) {
        if (this.isConnectedToClient(clientId)) {
            logger.debug("JAVA reply {}", message);
            this.reply(clientId, message.serializeToString());
        }
    }

    public void setCallback(NodeCallbackHandler cbOnDeliver) {
        this.cbOnDeliver = cbOnDeliver;
    }

    public McastNodeJNI(int groupId, int nodeId, String mconfig, String pconfig) {
        this.jni_init(groupId, nodeId, mconfig, pconfig);
    }

    public void start() {
        this.jni_start();
    }

    @Override
    public void run() {
        this.start();
    }

}

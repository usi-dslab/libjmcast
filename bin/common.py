import inspect
import json
import logging
import os
import shlex
import socket
import subprocess
import sys
import threading
from string import Template

__author__ = 'longle'

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')


class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            logging.debug('Thread started')
            run_args = shlex.split(self.cmd)
            self.process = subprocess.Popen(run_args)
            self.process.communicate()
            logging.debug('Thread finished')

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            logging.debug('Terminating process')
            self.process.terminate()
            thread.join()
        return self.process.returncode


class LauncherThread(threading.Thread):
    def __init__(self, clist):
        threading.Thread.__init__(self)
        self.cmdList = clist

    def run(self):
        for cmd in self.cmdList:
            logging.debug("Executing: %s", cmd["cmdstring"])
            if "output" in cmd and cmd["output"] is not None:
                sshcmdbg_streamoutput(cmd["node"], cmd["cmdstring"], cmd["output"])
            else:
                sshcmdbg(cmd["node"], cmd["cmdstring"])


def script_dir():
    return os.path.dirname(os.path.abspath(inspect.getsourcefile(lambda _: None)))


def sshcmd(node, cmdstring, timeout=None):
    finalstring = "ssh -o StrictHostKeyChecking=no " + node + " " + REMOTE_ENV + " \"" + cmdstring + "\""
    logging.debug(finalstring)
    cmd = Command(finalstring)
    return cmd.run(timeout)


def localcmd(cmdstring, timeout=None):
    logging.debug("localcmd:%s", cmdstring)
    cmd = Command(cmdstring)
    return cmd.run(timeout)


def sshcmdbg(node, cmdstring):
    cmd = "ssh -o StrictHostKeyChecking=no " + node + " " + REMOTE_ENV + " \"" + cmdstring + "\" &"
    logging.debug("sshcmdbg: %s", cmd)
    os.system(cmd)


def sshcmdbg_streamoutput(node, cmdstring, output):
    cmd = "ssh -o StrictHostKeyChecking=no " + node + " " + REMOTE_ENV + " \"" + cmdstring + "\" >> " + output + " &"
    logging.debug("sshcmdbg: %s", cmd)
    os.system(cmd)


def localcmdbg(cmdstring):
    logging.debug("localcmdbg: %s", cmdstring)
    os.system(cmdstring + " &")


def read_json_file(file_name):
    file_stream = open(file_name)
    content = json.load(file_stream)
    file_stream.close()
    return content


def sarg(i):
    return sys.argv[i]


def iarg(i):
    return int(sarg(i))


def farg(i):
    return float(sarg(i))


# available machines
def noderange(first, last, exclude=[]):
    return ["192.168.3." + str(val) for val in
            [node for node in range(first, last + 1) if node not in EXLCUDE_NODES]]


def render_template(ftpl, value, fout):
    # open the file
    filein = open(ftpl)
    # read it
    src = Template(filein.read())
    result = src.substitute(value)
    fileout = open(fout, "w")
    fileout.write(result)
    fileout.close()


localhostNodes = []
for i in range(1, 100):
    localhostNodes.append("localhost")

LOCALHOST = False

if socket.gethostname() in ['longs-usi-mbp.mobile.usilu.net', 'longs-mbp.mobile.usilu.net',
                            "Longs-MBP.fritz.box"]:
    LOCALHOST = True

# parameters
GLOBAL_HOME = os.path.normpath(script_dir() + '/../../')
HOME = os.path.normpath(script_dir() + '/../')
DEPENDENCIES_DIR = os.path.normpath(GLOBAL_HOME + '/dependencies/*')

if LOCALHOST:
    LOG4J = os.path.normpath(HOME + '/bin/log4jDebug.xml')
else:
    LOG4J = os.path.normpath(HOME + '/bin/log4j.xml')

LIBMCAST_HOME = os.path.normpath(GLOBAL_HOME + '/libjmcast')
LIBMCAST_CP = os.path.normpath(LIBMCAST_HOME + '/target/classes')
LIBMCAST_PAXOS_PROCESS = os.path.normpath(LIBMCAST_HOME + '/libmcast/build/local/bin/proposer-acceptor')
LIBMCAST_NODE_PROCESS = os.path.normpath(LIBMCAST_HOME + '/libmcast/build/local/bin/node-amcast')
LIBMCAST_CLIENT_PROCESS = os.path.normpath(LIBMCAST_HOME + '/libmcast/build/local/bin/client-mcast')

SENSE_HOME = os.path.normpath(GLOBAL_HOME + '/sense')
SENSE_CP = os.path.normpath(SENSE_HOME + '/target/classes')

NETWRAPPER_HOME = os.path.normpath(GLOBAL_HOME + '/netwrapper')
NETWRAPPER_CP = os.path.normpath(NETWRAPPER_HOME + '/target/classes')
SENSE_CLASS_GATHERER = "DataGatherer"
# SENSE_GATHERER_PORT = 60000
# SENSE_WARMUP_TIME = 0
# SENSE_DURATION = 380 if localhost else 500

JAVA_BIN = 'java -XX:+UseG1GC -Xmx8g -Dlog4j.configuration=file:' + LOG4J
_class_path = [os.path.normpath(GLOBAL_HOME + '/dependencies/guava-19.0.jar'), LIBMCAST_CP, SENSE_CP, NETWRAPPER_CP, DEPENDENCIES_DIR]

JAVA_CLASSPATH = '-cp \'' + ':'.join([str(val) for val in _class_path]) + '\''

# availableNodes = noderange(1,40) + noderange(66,72) + noderange(74,75)
EXLCUDE_NODES = [21, 40, 68, 73, 71, 41, 75, 78, 63, 80]
BEGIN_NODE = 1
END_NODE = 60

SENSE_GATHERER_PORT = 60000
SENSE_WARMUP_TIME = 600000
SENSE_DURATION = 60
SENSE_CLIENT_GATHERER_PATH = os.path.normpath(script_dir() + '/logs/client')
SENSE_GATHERER_PATH = os.path.normpath(script_dir() + '/logs/aggregated')

REMOTE_ENV = "" if LOCALHOST else "LD_LIBRARY_PATH=/home/long/.local/lib"

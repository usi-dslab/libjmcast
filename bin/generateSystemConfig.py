import common
import numpy as np

REPLICA_BASE_PORT = 50000
GROUP_LEARNER_BASE_PORT = 60000


def generate_paxos_config(num_group, num_replica, node_available, process_per_node=1):
    i_n = 0
    nodes = []
    for node in node_available:
        for i_p in range(process_per_node):
            nodes.append(node)
    ret = []
    for i_g in range(num_group):
        str_replica = []
        processes = []
        for i_r in range(num_replica):
            record = "replica " + str(i_r) + " " + nodes[i_n] + " " + str((REPLICA_BASE_PORT + i_n))
            str_replica.append(record)
            if nodes[i_n] in node_available: node_available.remove(nodes[i_n])
            processes.append({
                'host': nodes[i_n],
                'args': str(i_r),
                'node_id': i_r
            })
            i_n += 1
        file_name = common.script_dir() + "/generated_config/paxos-" + str(num_group) + "g" + str(
            num_replica) + "p-group" + str(i_g) + ".conf"
        common.render_template("paxos.conf.tpl", {"replicas": '\n'.join(str_replica)}, file_name)
        ret.append({
            'group': i_g,
            'processes': processes,
            'paxos_config_file': file_name
        })
    return ret


def generate_mcast_config(num_group, num_replica, node_available, process_per_node=1):
    i_n = 0
    nodes = []
    for node in node_available:
        for i_p in range(process_per_node):
            nodes.append(node)

    str_replica = []
    ret = []
    file_name = common.script_dir() + "/generated_config/mcast-" + str(num_group) + "g" + str(num_replica) + "p.conf"
    for i_g in range(num_group):
        processes = []
        for i_r in range(num_replica):
            str_replica.append("node " + str(i_g) + " " + str(i_r) + " " + nodes[i_n] + " "
                               + str((GROUP_LEARNER_BASE_PORT + i_n)))
            if nodes[i_n] in node_available: node_available.remove(nodes[i_n])
            processes.append({
                'host': nodes[i_n],
                'group_id': i_g,
                'node_id': i_r,
                'args': str(i_g) + " " + str(i_r)
            })
            i_n += 1
        ret.append({
            'group': i_g,
            'processes': processes,
            'mcast_config_file': file_name
        })

    common.render_template("mcast.conf.tpl", {"learners": '\n'.join(str_replica)}, file_name)
    return ret


def generate_config(num_group, num_acceptor, num_learner, node_available, paxos_process_per_node=1, learner_per_node=1):
    paxos_processes = generate_paxos_config(num_group, num_acceptor, node_available, paxos_process_per_node)
    server_processes = generate_mcast_config(num_group, num_learner, node_available, learner_per_node)
    ret = []
    for i_g in range(num_group):
        for process in paxos_processes[i_g]['processes']:
            process['paxos_config_file'] = paxos_processes[i_g]['paxos_config_file']
            process['type'] = 'paxos'
            ret.append(process)
        for process in server_processes[i_g]['processes']:
            process['type'] = 'mcast'
            process['paxos_config_file'] = paxos_processes[i_g]['paxos_config_file']
            process['mcast_config_file'] = server_processes[i_g]['mcast_config_file']
            ret.append(process)
    return ret

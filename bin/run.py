#!/usr/bin/python

import os
import time

import common
import generateSystemConfig

NUM_CLIENTS = 1
NUM_GROUPS = 1
NUM_ACCEPTOR = 3
NUM_REPLICA = 2

NODE_BEGIN = 2
NODE_END = 40
NODE_EXCLUDE = [1, 68, 73, 71, 41, 75, 78, 63, 80]

NUM_PAXOS_PROCESS_PER_NODE = 2
NUM_LEARNER_PROCESS_PER_NODE = 1

MCAST_GLOBAL_RATE = 0

SENSE_DURATION = 600
localhosts = []

log_path = os.path.normpath(
    common.script_dir() + '/logs/' + str(NUM_CLIENTS) + "c_" + str(NUM_GROUPS) + "g_" + str(NUM_REPLICA) + "r_" +
    str(MCAST_GLOBAL_RATE) + "a")


def run():
    for i in range(1, 50):
        localhosts.append("127.0.0.1")
    node_available = localhosts if common.LOCALHOST else common.noderange(NODE_BEGIN, NODE_END, NODE_EXCLUDE)

    processes = generateSystemConfig.generate_config(NUM_GROUPS, NUM_ACCEPTOR, NUM_REPLICA, node_available,
                                                     NUM_PAXOS_PROCESS_PER_NODE, NUM_LEARNER_PROCESS_PER_NODE)
    gatherer_node = node_available[0]
    del node_available[0]

    client_node = node_available[0]
    del node_available[0]

    testServerClass = "ch.usi.dslab.lel.jmcast.sample.Server"
    testClientClass = "ch.usi.dslab.lel.jmcast.sample.BenchClient"
    cmdList = []

    # deploy leaarner
    paxosCmdList= []
    serverCmdList= []
    for process in processes:
        if process['type'] is 'paxos':
            launchNodeCmdPieces = [common.LIBMCAST_PAXOS_PROCESS, process['args'], process['paxos_config_file'],
                                   "> /tmp/px00.log"]
            launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
            paxosCmdList.append({"node": process['host'], "cmdstring": launchNodeCmdString})
        elif process['type'] is 'mcast':
            launchNodeCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, testServerClass, process['group_id'],
                                   process['node_id'], process['mcast_config_file'], process['paxos_config_file'],
                                   gatherer_node, common.SENSE_GATHERER_PORT, common.SENSE_DURATION,
                                   log_path]
            launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
            mcast_config_file = process['mcast_config_file']
            serverCmdList.append({"node": process['host'], "cmdstring": launchNodeCmdString})

    server_thread = common.LauncherThread(paxosCmdList)
    server_thread.start()
    server_thread.join()

    time.sleep(5)

    server_thread = common.LauncherThread(serverCmdList)
    server_thread.start()
    server_thread.join()

    time.sleep(5)

    cmdList = []
    # deploy gatherer
    logsargs = ['throughput', 'jmcast_learner', str(NUM_REPLICA), 'throughput', 'jmcast_client', str(1), 'latency',
                'jmcast_client', str(1)]
    logsargs = " ".join([str(val) for val in logsargs])
    launchNodeCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, common.SENSE_CLASS_GATHERER,
                           common.SENSE_GATHERER_PORT,
                           common.SENSE_GATHERER_PATH, logsargs]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": gatherer_node, "cmdstring": launchNodeCmdString})

    # deploy client
    launchNodeCmdPieces = [common.JAVA_BIN, common.JAVA_CLASSPATH, testClientClass, 1, mcast_config_file,
                           MCAST_GLOBAL_RATE,
                           gatherer_node, common.SENSE_GATHERER_PORT, SENSE_DURATION, log_path]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": client_node, "cmdstring": launchNodeCmdString})

    thread = common.LauncherThread(cmdList)
    thread.start()
    thread.join()


if __name__ == '__main__':
    run()

#!/usr/bin/python

import common
import generateSystemConfig
import time
import os

NUM_CLIENTS = 1
NUM_GROUPS = 1
NUM_ACCEPTOR = 3
NUM_REPLICA = 2

NODE_BEGIN = 2
NODE_END = 40
NODE_EXCLUDE = [1, 68, 73, 71, 41, 75, 78, 63, 80]

NUM_PAXOS_PROCESS_PER_NODE = 2
NUM_LEARNER_PROCESS_PER_NODE = 1

MCAST_GLOBAL_RATE = 0

SENSE_DURATION = 300
localhosts = []

log_path = os.path.normpath(
    common.script_dir() + '/logs/amcast_' + str(NUM_CLIENTS) + "c_" + str(NUM_GROUPS) + "g_" + str(NUM_REPLICA) + "r_" +
    str(MCAST_GLOBAL_RATE) + "a")


def run():
    for i in range(1, 50):
        localhosts.append("127.0.0.1")
    node_available = localhosts if common.LOCALHOST else common.noderange(NODE_BEGIN, NODE_END, NODE_EXCLUDE)

    processes = generateSystemConfig.generate_config(NUM_GROUPS, NUM_ACCEPTOR, NUM_REPLICA, node_available,
                                                     NUM_PAXOS_PROCESS_PER_NODE, NUM_LEARNER_PROCESS_PER_NODE)
    gatherer_node = node_available[0]
    del node_available[0]

    client_node = node_available[0]
    del node_available[0]

    cmdList = []

    # deploy leaarner
    common.localcmd("mkdir -p " + log_path)
    for process in processes:
        if process['type'] is 'paxos':
            launchNodeCmdPieces = [common.LIBMCAST_PAXOS_PROCESS, process['args'], process['paxos_config_file'],
                                   "> /tmp/px" + str(process['args']) + ".log"]
            launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
            output = None
        elif process['type'] is 'mcast':
            filename = log_path + "/learner_" + str(process['group_id']) + "_" + str(process['node_id']) + ".log"
            launchNodeCmdPieces = [common.LIBMCAST_NODE_PROCESS, process['group_id'], process['node_id'],
                                   process['mcast_config_file'], process['paxos_config_file'], "|& tee ", filename]
            launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
            mcast_config_file = process['mcast_config_file']
            output = filename
        # cmdList.append({"node": process['host'], "cmdstring": launchNodeCmdString, "output": output})
        cmdList.append({"node": process['host'], "cmdstring": launchNodeCmdString})

    server_thread = common.LauncherThread(cmdList)
    server_thread.start()
    server_thread.join()

    time.sleep(5)
    cmdList = []

    # deploy client
    launchNodeCmdPieces = [common.LIBMCAST_CLIENT_PROCESS, mcast_config_file, "-a", MCAST_GLOBAL_RATE]
    launchNodeCmdString = " ".join([str(val) for val in launchNodeCmdPieces])
    cmdList.append({"node": gatherer_node, "cmdstring": launchNodeCmdString})

    thread = common.LauncherThread(cmdList)
    thread.start()
    thread.join()


if __name__ == '__main__':
    run()

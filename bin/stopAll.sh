#!/bin/bash
pkill java
pkill -9 proposer-acceptor
ps | grep proposer-acceptor | awk '{print $1}' | xargs kill -9
ps | grep node- | awk '{print $1}' | xargs kill -9
tmux kill-session -t amcast
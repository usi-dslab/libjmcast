#!/usr/bin/env bash
cd libmcast
mkdir build
cd build
rm -rf *
cmake .. -DCMAKE_INSTALL_PREFIX=$PWD/local -DLIBPAXOS_ROOT=/Users/longle/.local
make
make install

cd ../..
mvn package


#cmake -DCMAKE_PREFIX_PATH=/home/long/.local ../ -DCMAKE_INSTALL_PREFIX=$PWD/local
# LibMCast

This is an implementation of a basic multicast mechanism.

LibMCast is divided in two libraries: libmcast and libevmcast. 

Libmcast (see ```mcast```) implements the core of the multicast mechanism and
related structures, and is not cluttered with network specific code.

Libevmcast (see ```evmcast``` and ```evamcast```) is the actual networked
multicast implementation.  This library is built on top of the libmcast and
[libevent][1].

## Building

These are the basic steps required to get and compile LibPaxos3

	git clone https://bitbucket.org/paulo_coelho/libmcast.git
	mkdir libmcast/build
	cd libmcast/build
	cmake ..
	make

LibMCast depends on [libevent][1] ,[msgpack][2] and [libpaxos][3]. By default,
LibMCast uses an in-memory storage.  You can store in disk if you have [lmdb][4] installed.
(see `sample/mcast-1g3p.conf` for configuration details).

LibMCast should compile on Linux and OS X.

### Useful build options

You pass options to cmake as follows: ```cmake -DOPTION=VALUE```

- ```LIBEVENT_ROOT=PATH``` -  point it to your installation of Libevent
- ```MSGPACK_ROOT=PATH``` - point it to your installation of MessagePack
- ```LIBPAXOS_ROOT=PATH``` - point it to your installation of LibPaxos3
- ```LMDB_ROOT=PATH``` - point it to your installation of LMDB (if you want to persistent storage)

## Running the examples

	cd libmcast/build/sample
        cp ../../sample/*conf .
        cp ../../sample/*sh .
        ./run-2g3p.sh amcast|mcast|famcast

## Configuration

See ```sample/*.conf``` for a sample configuration file.

##  Unit tests (NOT DEVELOPED YET)

Unit tests will depend on the [Google Test][5] library. Execute the tests using
```make test``` in your build directory, or run ```runtest``` from
```build/unit``` for  detailed output.

## License

The library is under development and not ready for distribution. All rights reserved to [USI][6].

[1]: http://www.libevent.org
[2]: http://www.msgpack.org
[3]: https://bitbucket.org/sciascid/libpaxos
[4]: https://github.com/LMDB/lmdb
[5]: https://github.com/google/googletest
[6]: http://inf.usi.ch

/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "amcast_data.h"
#include "carray.h"
#include <assert.h>
#include <stdlib.h>

// for to_order set
struct msg_array_t
{
  int            head;
  int            tail;
  int            size;
  int            count;
  mcast_message* array;
};

struct proposal_t
{
  unsigned       k_p;   // proposal identifier
  int            size;  // capacity of the array
  int            count; // number of messages
  mcast_message* msgs;  // array of messages
};

KHASH_MAP_INIT_INT64(proposal, struct proposal_t*)

struct pending_t
{
  struct carray* src;
  kh_proposal_t* dst;
};

struct msg_status_t
{
  m_uid_t  uid;           // unique id of the message
  m_ts_t   ts;            // timestamp: updated only if the new one is greater
  unsigned group_bits;    // snd-hard from each group: if my group is set, this means set-hard was decided!
  unsigned expected_bits; // expected bits to be set
};

struct ll_node_t
{
  struct msg_status_t ms;
  struct ll_node_t*   next;
  struct ll_node_t*   previous;
};

KHASH_MAP_INIT_INT64(node, struct ll_node_t*)

struct ll_msg_t
{
  struct ll_node_t* head; // entry point
  kh_node_t*        ht;   // hash table of ll_node_t
  int               size; // size of list
};

struct deliverable_t
{
  struct carray *uid, *ts;
};
//--------------------------------------

static void msg_array_grow(struct msg_array_t* a);

static mcast_message* msg_array_at(struct msg_array_t* a, int i);

static struct msg_array_t*
msg_array_new(int size)
{
  struct msg_array_t* a;
  a = malloc(sizeof(struct msg_array_t));
  assert(a != NULL);
  a->head = 0;
  a->tail = 0;
  a->size = size;
  a->count = 0;
  a->array = malloc(sizeof(mcast_message) * a->size);
  assert(a->array != NULL);
  return a;
}

static void
msg_array_free(struct msg_array_t* a)
{
  free(a->array);
  free(a);
}

static int
msg_array_push_back(struct msg_array_t* a, mcast_message* p)
{
  if (a->count == a->size) // if full
    msg_array_grow(a);
  mcast_message_copy(&a->array[a->tail], p, 0);
  a->tail = (a->tail + 1) % a->size;
  a->count++;
  return 0;
}

static void
msg_array_grow(struct msg_array_t* a)
{
  int                 i;
  struct msg_array_t* tmp = msg_array_new(a->size * 2);
  for (i = 0; i < a->count; i++)
    msg_array_push_back(tmp, msg_array_at(a, i));
  free(a->array);
  a->head = 0;
  a->tail = tmp->tail;
  a->size = tmp->size;
  a->array = tmp->array;
  free(tmp);
}

static mcast_message*
msg_array_at(struct msg_array_t* a, int i)
{
  return &(a->array[(a->head + i) % a->size]);
}

// --------

/**
 * TO_ORDER set (array)
 */
to_order_t*
to_order_new(int initial_size)
{
  to_order_t* s = msg_array_new(initial_size);
  return s;
}

void
to_order_free(to_order_t* s)
{
  msg_array_free(s);
}

void
to_order_add(to_order_t* s, mcast_message* m)
{
  msg_array_push_back(s, m);
}

void
to_order_empty(to_order_t* s)
{
  s->head = s->tail = s->count = 0;
}

int
to_order_get_all(to_order_t* s, mcast_message** m)
{
  *m = s->array;
  return s->count;
}

int
to_order_count(to_order_t* s)
{
  return s->count;
}

// --------

/**
 * PENDING set (hashtable of arrays)
 */
pending_t*
pending_new(int initial_size, int parallel_instances)
{
  int               i = 0;
  struct pending_t* s = malloc(sizeof(struct pending_t));
  s->src = carray_new(parallel_instances + 3);
  for (; i < carray_size(s->src); i++) {
    struct proposal_t* p = malloc(sizeof(struct proposal_t));
    p->k_p = 0;
    p->size = initial_size;
    p->count = 0;
    p->msgs = malloc(sizeof(mcast_message) * initial_size);
    carray_push_back(s->src, p);
  }
  s->dst = kh_init_proposal();
  return s;
}

void
pending_free(pending_t* s)
{
  struct proposal_t* p;
  kh_foreach_value(s->dst, p, free(p->msgs));
  kh_foreach_value(s->dst, p, free(p));
  carray_free(s->src);
  kh_destroy_proposal(s->dst);
  free(s);
}

void
pending_add(pending_t* s, unsigned k_p, mcast_message* m, int size)
{
  assert(!carray_empty(s->src));
  struct proposal_t* p = carray_pop_front(s->src);
  khiter_t           k = kh_get_proposal(s->dst, k_p);
  int                rv = 0;
  assert(k == kh_end(s->dst));
  if (p->size < size) {
    mcast_log_info("PENDING set: reallocating memory for proposal %u: %d messages", k_p, size);
    p->msgs = realloc(p->msgs, sizeof(mcast_message) * (size + 100));
    p->size = size + 100;
  }
  memcpy(p->msgs, m, sizeof(mcast_message) * size);
  p->count = size;
  k = kh_put_proposal(s->dst, k_p, &rv);
  kh_value(s->dst, k) = p;
}

int
pending_get(pending_t* s, unsigned k_p, mcast_message** m)
{
  khiter_t           k = kh_get_proposal(s->dst, k_p);
  struct proposal_t* p = NULL;
  *m = NULL;
  if (k == kh_end(s->dst))
    return 0;
  p = kh_value(s->dst, k);
  *m = p->msgs;
  return p->count;
}

void
pending_del(pending_t* s, unsigned k_p)
{
  khiter_t           k = kh_get_proposal(s->dst, k_p);
  struct proposal_t* p = NULL;
  if (k == kh_end(s->dst))
    return;
  p = kh_value(s->dst, k);
  p->count = 0;
  p->k_p = 0;
  carray_push_back(s->src, p);
  kh_del_proposal(s->dst, k);
}

// --------

/**
 * ORDERED set (hashtable of status)
 */
ordered_t*
ordered_new()
{
  return kh_init_msg();
}

void
ordered_free(ordered_t* s)
{
  struct msg_status_t* c;
  kh_foreach_value(s, c, free(c));
  kh_destroy_msg(s);
}

void
ordered_add(ordered_t* s, mcast_message* m)
{
  struct msg_status_t* c = NULL;
  khiter_t             k = kh_get_msg(s, m->uid);
  int                  rv = 0;

  if (k == kh_end(s)) {
    c = malloc(sizeof(struct msg_status_t));
    memset(c, 0, sizeof(struct msg_status_t));
    c->uid = m->uid;
    // c->expected_bits is not used in ORDERED set
    k = kh_put_msg(s, m->uid, &rv);
    kh_value(s, k) = c;
  } else
    c = kh_value(s, k);

  if (c->ts < m->timestamp)
    c->ts = m->timestamp;
  c->group_bits = c->group_bits | (1 << m->from_group);
}

void
ordered_del(ordered_t* s, m_uid_t uid)
{
  struct msg_status_t* c = NULL;
  khiter_t             k = kh_get_msg(s, uid);

  if (k != kh_end(s)) {
    c = kh_value(s, k);
    free(c);
    kh_del_msg(s, k);
  }
}

int
ordered_has(ordered_t* s, mcast_message_type t, m_uid_t uid, m_gid_t group)
{
  struct msg_status_t* c = NULL;
  khiter_t             k = kh_get_msg(s, uid);

  if (k != kh_end(s)) {
    assert(t == MCAST_SYNC_HARD || t == MCAST_SET_HARD);
    c = kh_value(s, k);
    if (c->group_bits == (1 << group))
      return 1;
  }
  return 0;
}

int
ordered_count(ordered_t* s)
{
  return kh_size(s);
}

// --------

/**
 * BUFFER set (hashtable of status)
 */
buff_t*
buffer_new()
{
  struct ll_msg_t* s = malloc(sizeof(struct ll_msg_t));

  s->size = 0;
  s->ht = kh_init_node();
  s->head = malloc(sizeof(struct ll_node_t));
  s->head->ms.uid = 0;
  s->head->ms.ts = 0;
  s->head->ms.group_bits = 0;
  s->head->ms.expected_bits = 0;
  s->head->next = s->head;
  s->head->previous = s->head;
  return s;
}

void
buffer_free(buff_t* s)
{
  struct ll_node_t* n;

  kh_foreach_value(s->ht, n, free(n));
  kh_destroy_node(s->ht);
  free(s->head);
  free(s);
}

static void
reposition_node(buff_t* s, struct ll_node_t* n, int is_new)
{
  if (!is_new) {
    if (s->size == 1 || n->next == s->head)
      return;

    struct ll_node_t* tmp = n->next;

    while (tmp != s->head && ((n->ms.ts > tmp->ms.ts) || (n->ms.ts == tmp->ms.ts && n->ms.uid > tmp->ms.uid))) {
      tmp = tmp->next;
    }

    if (tmp != n->next) { // if I need to reposition
      n->previous->next = n->next;
      n->next->previous = n->previous;

      n->previous = tmp->previous;
      n->next = tmp;
      tmp->previous->next = n;
      tmp->previous = n;
    }
  } else {
    n->next = s->head->next;
    n->previous = s->head;
    s->head->next = n;
    n->next->previous = n;
    reposition_node(s, n, 0);
  }
}

void
buffer_add(buff_t* s, mcast_message* m)
{
  struct ll_node_t* c = NULL;
  khiter_t          k = kh_get_node(s->ht, m->uid);
  int               is_new = 0, rv = 0, i = 0;

  if (k == kh_end(s->ht)) {
    c = malloc(sizeof(struct ll_node_t));
    c->next = c->previous = NULL;
    c->ms.uid = m->uid;
    c->ms.expected_bits = c->ms.group_bits = 0;

    for (; i < m->to_groups_len; i++)
      c->ms.expected_bits = c->ms.expected_bits | (1 << m->to_groups[i]);

    k = kh_put_node(s->ht, m->uid, &rv);
    kh_value(s->ht, k) = c;
    is_new = 1; // insert node
    s->size++;
  } else
    c = kh_value(s->ht, k);

  c->ms.group_bits = c->ms.group_bits | (1 << m->from_group);

  if (is_new || c->ms.ts < m->timestamp) {
    c->ms.ts = m->timestamp;
    reposition_node(s, c, is_new);
  }
}

static struct ll_node_t*
buffer_try_to_antecipate(buff_t* s, conflict_check_function_inner f, void* arg)
{
  struct ll_node_t *p = s->head->next, *n = NULL;

  while (p->ms.expected_bits != p->ms.group_bits) { // look for FINAL msgs
    p = p->next;
  }

  if (p == s->head) // if back to head, return the first
    return p->next;

  // I found a FINAL msg, check for conflicts
  n = p;
  while (p->previous != s->head) {
    if (f(arg, p->previous->ms.uid, n->ms.uid)) {
      return s->head->next;
    }

    p = p->previous;
  }
  return n;
}

int
buffer_get_next(buff_t* s, mcast_message* m, conflict_check_function_inner f, void* arg)
{
  if (s->size == 0)
    return 0;

  struct ll_node_t* n = s->head->next;

  // if there's a conflict check function and the size is greater than one and
  // first message's type is not FINAL, then try to antecipate
  if (f != NULL && n->ms.group_bits != n->ms.expected_bits && s->size > 1) {
    n = buffer_try_to_antecipate(s, f, arg);
  }

  m->uid = n->ms.uid;
  m->type = (n->ms.group_bits == n->ms.expected_bits) ? MCAST_FINAL : MCAST_SYNC_HARD;
  m->timestamp = n->ms.ts;
  m->from_group = m->from_node = 0;
  m->value.mcast_value_len = 0;
  m->value.mcast_value_val = NULL;
  return 1;
}

void
buffer_del(buff_t* s, m_uid_t uid)
{
  khiter_t          k = kh_get_node(s->ht, uid);
  struct ll_node_t* n = kh_value(s->ht, k);

  if (k == kh_end(s->ht))
    return;

  n->next->previous = n->previous;
  n->previous->next = n->next;
  free(n);
  kh_del_node(s->ht, k);
  s->size--;
}

int
buffer_count(buff_t* s)
{
  return s->size;
}

void
buffer_print(buff_t* s)
{
  struct ll_node_t* p = s->head->next;

  mcast_log_info("BUFFER set:");
  while (p != s->head) {
    mcast_log_info("\t--- (uid, ts, expected, received) = (%lu, %u, %u, %u)", p->ms.uid, p->ms.ts, p->ms.expected_bits,
                   p->ms.group_bits);
    p = p->next;
  }
}

// --------

/**
 * DELIVERABLE set: array of messages unique identifiers
 */
deliverable_t*
deliverable_new(int initial_size)
{
  struct deliverable_t* s = malloc(sizeof(struct deliverable_t));

  s->ts = carray_new(initial_size);
  s->uid = carray_new(initial_size);
  return s;
}

void
deliverable_free(deliverable_t* s)
{
  carray_free(s->ts);
  carray_free(s->uid);
  free(s);
}

void
deliverable_push_back(deliverable_t* s, mcast_message* m)
{
  m_uid_t ts = m->timestamp;
  carray_push_back(s->ts, (void*)ts);
  carray_push_back(s->uid, (void*)m->uid);
}

int
deliverable_pop_front(deliverable_t* s, mcast_message* m)
{
  if (carray_empty(s->uid))
    return 0;

  m->timestamp = (m_ts_t)carray_pop_front(s->ts);
  m->uid = (m_uid_t)carray_pop_front(s->uid);
  m->type = MCAST_FINAL;
  return 1;
}

int
deliverable_check_front(deliverable_t* s, mcast_message* m)
{
  if (carray_empty(s->uid))
    return 0;

  m->timestamp = (m_ts_t)carray_check_front(s->ts);
  m->uid = (m_uid_t)carray_check_front(s->uid);
  m->type = MCAST_FINAL;
  return 1;
}

int
deliverable_count(deliverable_t* s)
{
  return carray_count(s->ts);
}

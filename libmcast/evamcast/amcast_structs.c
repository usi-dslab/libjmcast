/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "amcast_structs.h"
#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * TO_ORDER
 */
void
to_order_init(struct to_order_t* to)
{
  to->instance = 0;
  to->count = 0;
}

void
to_order_add(struct to_order_t* to, size_t instance, m_ts_t ts, m_uid_t uid, m_gid_t* dst, size_t dst_size,
             mcast_message_type type)
{
  to->instance = instance;
  to->msgs[to->count].ts = ts;
  to->msgs[to->count].uid = uid;
  to->msgs[to->count].type = type;
  to->msgs[to->count].dst_size = dst_size;
  memcpy(to->msgs[to->count].dst, dst, dst_size * sizeof(m_gid_t));
  to->count++;
}

int
to_order_get_all(struct to_order_t* to, struct prop_t** p)
{
  *p = to->msgs;
  return to->count;
}

int
to_order_count(struct to_order_t* to)
{
  return to->count;
}

int
to_order_cmp(struct to_order_t* to, struct prop_t* msgs, size_t count)
{
  if (to->count != count)
    return 1;

  return memcmp(to->msgs, msgs, sizeof(struct prop_t) * count);
}

/*
 * PENDING
 */
void
pending_init(struct pending_t* p)
{
  p->count = p->head = p->tail = 0;
}

size_t
pending_push(struct pending_t* p, struct to_order_t* to, char** buf)
{
  p->pends[p->tail].instance = to->instance;
  p->pends[p->tail].count = to->count;
  memcpy(p->pends[p->tail].msgs, to->msgs, to->count * sizeof(struct prop_t));
  *buf = (char*)p->pends[p->tail].msgs;
  p->tail = (p->tail + 1) % PENDING_SIZE;
  p->count++;
  return (to->count * sizeof(struct prop_t));
}

int
pending_pop(struct pending_t* p, struct to_order_t** to)
{
  if (p->count) {
    /*
     to->instance = p->pends[p->head].instance;
     to->count = p->pends[p->head].count;
     memcpy(to->msgs, p->pends[p->head].msgs, to->count * sizeof(struct prop_t));
     */
    *to = &p->pends[p->head];
    p->head = (p->head + 1) % PENDING_SIZE;
    p->count--;
    return 1;
  }
  return 0;
}

/*
 * BUFFER
 */
static int
prop_comparator(struct prop_t* left, struct prop_t* right)
{
  if (left->ts < right->ts)
    return -1;
  if (left->ts > right->ts)
    return 1;
  if (left->uid < right->uid)
    return -1;
  if (left->uid > right->uid)
    return 1;
  return 0;
}

static int
mybsearch(struct prop_t a[], struct prop_t* item, int low, int high, int* found)
{
  *found = 0;
  int comp = 0;
  if (high <= low) {
    comp = prop_comparator(item, &a[low]);
    if (comp >= 0) {
      *found = 1 - comp;
      return (low + 1);
    }
    return low;
  }

  int mid = (low + high) / 2;
  comp = prop_comparator(item, &a[mid]);

  if (comp == 0) {
    *found = 1;
    return mid + 1;
  }

  if (comp > 0)
    return mybsearch(a, item, mid + 1, high, found);

  return mybsearch(a, item, low, mid - 1, found);
}

/* DEBUG PURPOSE ONLY */
/*
static void
print_buffer(struct buffer_t* b)
{
  int i;
  printf("Buffer with %zu messages.\n", b->count);
  for (i = 0; i < b->count; i++) {
    printf("\t(ts, uid, type) = (%u, %" PRIu64 ", %s)\n", b->msgs[i].ts, b->msgs[i].uid,
           mcast_message_get_type_as_string(b->msgs[i].type));
  }
  printf("\n");
}
*/

void
buffer_init(struct buffer_t* b)
{
  b->count = 0;
}

void
buffer_add(struct buffer_t* b, m_ts_t new_ts, m_ts_t old_ts, m_uid_t uid, mcast_message_type type)
{
  int           pos = 0, found = 0;
  struct prop_t p = {.ts = new_ts, .uid = uid, .type = type };

  if (b->count == 0) {
    b->msgs[0] = p;
    b->count++;
  } else {
    if (old_ts) {
      p.ts = old_ts;
      pos = mybsearch(b->msgs, &p, 0, b->count - 1, &found);
      assert(found);
      if (old_ts >= new_ts) {
        b->msgs[pos - 1].type = type;
        return;
      } else {
        p.ts = new_ts;
        memmove(&b->msgs[pos - 1], &b->msgs[pos], (b->count - pos) * sizeof(struct prop_t));
        b->count--;
      }
    }

    pos = 0;
    if (b->count > 0) {
      pos = mybsearch(b->msgs, &p, 0, b->count - 1, &found);
      if (pos < b->count)
        memmove(&b->msgs[pos + 1], &b->msgs[pos], (b->count - pos) * sizeof(struct prop_t));
    }
    b->msgs[pos] = p;
    b->count++;
  }
}

void
buffer_del(struct buffer_t* b, m_ts_t ts, m_uid_t uid)
{
  int           found = 0, pos;
  struct prop_t p = { ts, uid, 0 };

  pos = mybsearch(b->msgs, &p, 0, b->count, &found);
  if (found) {
    memmove(&b->msgs[pos - 1], &b->msgs[pos], (b->count - pos) * sizeof(struct prop_t));
    b->count--;
  }
}

int
buffer_get_front(struct buffer_t* b, m_ts_t* ts, m_uid_t* uid, conflict_check_function_inner ccf, void* data_src)
{
  int i, j;

  if (b->count) {
    *ts = b->msgs[0].ts;
    *uid = b->msgs[0].uid;
    if (b->msgs[0].type == MCAST_FINAL) {
      b->count--;
      memmove(b->msgs, &b->msgs[1], b->count * sizeof(struct prop_t));
      return 1;
    } else if (b->count > 1 && ccf != NULL) {
      // check conflict
      mcast_log_info("EXPERIMENTAL: Trying to find a message which can be anticipated");
      for (i = 1; i < b->count; i++) {
        if (b->msgs[i].type == MCAST_FINAL) {
          *uid = b->msgs[i].uid;
          *ts = b->msgs[i].ts;
          for (j = 0; j < i; j++)
            if (ccf(data_src, *uid, b->msgs[j].uid) == 1) // if they conflict, then exit
              return 0;

          mcast_log_info("EXPERIMENTAL: Message %" PRIu64 " is being anticipated", *uid);
          b->count--;
          memmove(&b->msgs[i], &b->msgs[i + 1], (b->count - i) * sizeof(struct prop_t));
          return 1;
        }
      }
    }
  }
  return 0;
}

/*
 * DELIVERABLE
 */
void
deliverable_init(struct deliverable_t* d)
{
  d->count = d->head = d->tail = 0;
}

void
deliverable_push(struct deliverable_t* d, m_ts_t ts, m_uid_t uid)
{
  d->msgs[d->tail].ts = ts;
  d->msgs[d->tail].uid = uid;
  d->msgs[d->tail].type = MCAST_FINAL;
  d->tail = (d->tail + 1) % MAX_AMC_STATE_SIZE;
  d->count++;
}

int
deliverable_pop(struct deliverable_t* d, m_ts_t* ts, m_uid_t* uid, int del)
{
  if (d->count) {
    *ts = d->msgs[d->head].ts;
    *uid = d->msgs[d->head].uid;
    if (del) {
      d->head = (d->head + 1) % MAX_AMC_STATE_SIZE;
      d->count--;
    }
    return 1;
  }
  return 0;
}

/*
 * AMC_STATE
 */
amc_state_t*
amc_state_new()
{
  return kh_init_state();
}

void
amc_state_free(amc_state_t* st)
{
  struct state_t* s;
  kh_foreach_value(st, s, free(s));
  kh_destroy_state(st);
}

void
amc_state_add(amc_state_t* st, m_uid_t uid, size_t dst_size)
{
  khiter_t        k = kh_get_state(st, uid);
  struct state_t* s;
  int             rv = 0;

  if (k == kh_end(st)) {
    s = malloc(sizeof(struct state_t));
    memset(s, 0, sizeof(struct state_t));
    s->uid = uid;
    s->type = MCAST_SET_HARD;
    s->type_soft = MCAST_SET_HARD;
    s->dst_size = dst_size;
    k = kh_put_state(st, uid, &rv);
    kh_value(st, k) = s;
  }
}

void
amc_state_del(amc_state_t* st, m_uid_t uid)
{
  khiter_t        k = kh_get_state(st, uid);
  struct state_t* s;

  if (k != kh_end(st)) {
    s = kh_value(st, k);
    kh_del_state(st, k);
    free(s);
  }
}

void
amc_state_update(amc_state_t* st, m_uid_t uid, m_ts_t ts, m_gid_t gid, mcast_message_type type, size_t dst_size,
                 m_ts_t* ts_before, mcast_message_type* type_after)
{
  khiter_t        k = kh_get_state(st, uid);
  struct state_t* s;
  int             rv = 0;

  if (k == kh_end(st)) {
    s = malloc(sizeof(struct state_t));
    memset(s, 0, sizeof(struct state_t));
    s->uid = uid;
    s->type = MCAST_SET_HARD;
    s->dst_size = dst_size;
    k = kh_put_state(st, uid, &rv);
    kh_value(st, k) = s;
  } else {
    s = kh_value(st, k);
  }

  *ts_before = s->ts;
  if (type == MCAST_FINAL) {
    s->ts = ts;
    s->type = type;
  } else if (s->tss[gid] == 0) {
    s->dst_count++;
    s->tss[gid] = ts;
    s->type = MCAST_SND_HARD;
    if (ts > s->ts)
      s->ts = ts;
    if (s->dst_size == s->dst_count) {
      s->type = MCAST_SYNC_HARD;
    }
  }
  *type_after = s->type;
}

void
amc_state_update_fast(amc_state_t* st, m_uid_t uid, m_ts_t ts, m_gid_t gid, mcast_message_type type, size_t dst_size,
                      m_ts_t* ts_before, mcast_message_type* type_after)
{
  khiter_t        k = kh_get_state(st, uid);
  struct state_t* s;
  int             rv = 0;

  if (k == kh_end(st)) {
    s = malloc(sizeof(struct state_t));
    memset(s, 0, sizeof(struct state_t));
    s->uid = uid;
    s->type = MCAST_SET_HARD;
    s->type_soft = MCAST_SET_HARD;
    s->dst_size = dst_size;
    k = kh_put_state(st, uid, &rv);
    kh_value(st, k) = s;
  } else {
    s = kh_value(st, k);
  }

  *ts_before = s->ts;

  switch (type) {
    case MCAST_FINAL:
      s->ts = ts;
      s->type = type;
      break;

    case MCAST_SND_HARD:
      if (s->tss[gid] == 0) {
        s->dst_count++;
        s->tss[gid] = ts;
        s->type = MCAST_SND_HARD;
        if (ts > s->ts)
          s->ts = ts;
        if (s->dst_size == s->dst_count) {
          if (s->type_soft == MCAST_SYNC_SOFT) {
            if (s->ts == s->ts_soft) {
              // mcast_log_debug("fast path (SS->SH) for message %llu", s->uid);
              s->type = MCAST_FINAL;
            } else {
              mcast_log_info("NO MATCH in fast path (SS->SH) for message %" PRIu64, s->uid);
              s->type = MCAST_SYNC_HARD;
            }
          }
        }
      }
      break;
    case MCAST_SND_SOFT:
      *ts_before = s->ts_soft;
      if (s->tss_soft[gid] == 0 && s->type_soft != MCAST_SYNC_SOFT) {
        s->dst_soft_count++;
        s->tss_soft[gid] = ts;
        s->type_soft = MCAST_SND_SOFT;
        if (ts > s->ts_soft)
          s->ts_soft = ts;
        if (s->dst_size == s->dst_soft_count) {
          // s->type_soft = MCAST_SYNC_SOFT;
          s->type = MCAST_SYNC_SOFT;
        }
      }
      break;
    case MCAST_SYNC_SOFT:
      s->ts_soft = ts;
      s->type_soft = MCAST_SYNC_SOFT;
      if (s->dst_size == s->dst_count) { // snd-hard from all g in m.dst
        if (s->ts == s->ts_soft) {
          // mcast_log_debug("fast path (SH->SS) for message %llu", s->uid);
          s->type = MCAST_FINAL;
        } else {
          mcast_log_info("NO MATCH in fast path (SH->SS) for message %" PRIu64, s->uid);
          s->type = MCAST_SYNC_HARD;
        }
      }
      break;
    default:
      mcast_log_info("STATE UPDATE SHOULD NOT BE '%s!", mcast_message_get_type_as_string(type));
  }

  *type_after = s->type;
}

/*
 * Copyright (c) 2015-2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Implementation of the fast algorithm,
 * providing atomic multicast in 4 communication steps (best case)
 */

#include "amcast_structs.h"
#include "evamcast.h"
#include "evfamcast.h"
#include "evmcast_internal.h"
#include "mcast_consensus.h"
#include "mcast_node.h"
#include <assert.h>
#include <errno.h>
#include <event2/event.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

struct evfamcast_node
{
  int                    id;
  int                    group_id;
  int                    leader_id;       // id of the group leader
  struct mcast_node*     state;           // keeps state of received messages
  struct evmcast_node*   mc_node;         // implements reliable multicast
  struct mcast_peers*    mcast_peers;     // Manages all connections
  struct timeval         proposal_tv;     // timeout between proposals
  struct event*          proposal_ev;     // proposal event
  unsigned int           k_p, k_d;        // last proposed and decided instances, respectively
  struct timeval         last_prop;       // last proposal timestamp
  m_ts_t                 c_h, c_s;        // timestamps: HARD and SOFT
  amc_state_t*           amc_st;          // state of each message
  struct to_order_t      to_order;        // TO_ORDER set
  struct pending_t       pending;         // PENDING/ORDERED set
  struct buffer_t        buffer;          // BUFFER set
  struct deliverable_t   deliverable;     // DELIVERABLE set
  int                    content_pending; // whether there is content pending for delivery
  struct mcast_consensus consensus;       // buffer to send proposes
};

/* fast algorithm tasks */
static void timed_events(evutil_socket_t fd, short event, void* arg);
static void handle_start(struct mcast_peer* p, mcast_message* m, void* arg);
static void handle_snd_soft(struct mcast_peer* p, mcast_message* m, void* arg);
static void handle_snd_hard(struct mcast_peer* p, mcast_message* m, void* arg);
static void try_to_propose(struct evfamcast_node* n, int timedout);
static void when_decided(unsigned k_d, char* value, size_t size, void* arg);
static void try_to_adeliver(struct evfamcast_node* n);

/* message exchange functions */
void
evfamcast_node_free_internal(struct evfamcast_node* n)
{
  if (n->mc_node)
    evmcast_node_free_internal(n->mc_node);

  amc_state_free(n->amc_st);
  event_free(n->proposal_ev);
  free(n);
}

void
evfamcast_node_free(struct evfamcast_node* n)
{
  if (n->consensus.handle)
    mcast_consensus_close(&n->consensus);

  mcast_peers_free(n->mcast_peers);
  evfamcast_node_free_internal(n);
}

struct evfamcast_node*
evfamcast_node_init_internal(int group_id, int id, struct evmcast_config* c, struct mcast_peers* mcast_peers,
                             mcast_deliver_function cb, void* arg)
{
  struct evfamcast_node* n;

  n = malloc(sizeof(struct evfamcast_node));
  memset(n, 0, sizeof(struct evfamcast_node));
  n->group_id = group_id;
  n->id = id;
  n->amc_st = amc_state_new();
  to_order_init(&n->to_order);
  pending_init(&n->pending);
  buffer_init(&n->buffer);
  deliverable_init(&n->deliverable);
  n->mc_node = evmcast_node_init_internal(group_id, id, c, mcast_peers, cb, arg);
  if (n->mc_node == NULL) {
    evfamcast_node_free_internal(n);
    return NULL;
  }

  n->state = evmcast_node_get_state(n->mc_node);
  n->mcast_peers = mcast_peers;
  mcast_peers_subscribe(mcast_peers, MCAST_START, handle_start, n);
  mcast_peers_subscribe(mcast_peers, MCAST_SND_SOFT, handle_snd_soft, n);
  mcast_peers_subscribe(mcast_peers, MCAST_SND_HARD, handle_snd_hard, n);

  // Setup proposal timeout
  n->proposal_tv.tv_sec = mcast_config.message_timeout / 1000000;
  n->proposal_tv.tv_usec = mcast_config.message_timeout % 1000000;
  n->proposal_ev = evtimer_new(mcast_peers_get_event_base(mcast_peers), timed_events, n);
  event_add(n->proposal_ev, &n->proposal_tv);
  mcast_log_info("Node %d started. Max proposing interval = %dsec, %dusec", n->id, n->proposal_tv.tv_sec,
                 n->proposal_tv.tv_usec);
  return n;
}

struct evfamcast_node*
evfamcast_node_init(int group_id, int id, const char* mcast_config_file, const char* paxos_config_file,
                    struct event_base* base, mcast_deliver_function cb, void* arg)
{
  struct evmcast_config* mconfig = evmcast_config_read(mcast_config_file);

  if (mconfig == NULL) {
    mcast_log_error("Error loading config file");
    return NULL;
  }
  // Check id validity of node and group
  if (id < 0 || id >= MAX_N_OF_NODES || group_id < 0 || group_id > MAX_N_OF_GROUPS) {
    mcast_log_error("Invalid mcast_node group_id/node_id: %d/%d", group_id, id);
    return NULL;
  }

  // Connect to nodes and start listening
  struct mcast_peers*    mcast_peers = mcast_peers_new(base, mconfig);
  struct evfamcast_node* n = evfamcast_node_init_internal(group_id, id, mconfig, mcast_peers, cb, arg);

  if (n != NULL) {
    // setup consensus
    n->consensus.handle = NULL;
    if (!mcast_consensus_init(&n->consensus, base)) {
      evfamcast_node_free(n);
      mcast_log_error("Could not start consensus infrastructure");
      return NULL;
    }

    mcast_consensus_connect_to_proposer(&n->consensus, id, paxos_config_file);
    mcast_consensus_start_learner(&n->consensus, paxos_config_file, when_decided, n);
  }
  evmcast_config_free(mconfig);
  return n;
}

static long
timeval_diff(struct timeval* t1)
{
  long           us;
  struct timeval t2;
  gettimeofday(&t2, NULL);
  us = (t2.tv_sec - t1->tv_sec) * 1e6;
  if (us < 0)
    return 0;
  us += (t2.tv_usec - t1->tv_usec);
  return us;
}

/*********************************
 * FAST ATOMIC MULTICAST RELATED *
 *********************************/

static void
timed_events(evutil_socket_t fd, short event, void* arg)
{
  struct evfamcast_node* n = arg;

  if (n->id == n->leader_id && timeval_diff(&(n->last_prop)) > mcast_config.message_timeout)
    try_to_propose(n, 1);

  event_add(n->proposal_ev, &n->proposal_tv);
}

/*
 * Task 1: When a new message of type START is r-delivered, it must
 * be added to the state and to the pending set (to_order) with type SET-HARD.
 *
 * when r-deliver(start, ?, ?, m)
 *   ToOrder <- ToOrder U (set-hard, g, ?, m)
 */
static void
handle_start(struct mcast_peer* p, mcast_message* m, void* arg)
{
  struct evfamcast_node* n = arg;

  if (!mcast_node_is_multicast(n->state, m->uid)) {
    mcast_log_debug("START: (uid, gid, nid) = (%" PRIu64 ", %d, %d)", m->uid, m->from_group, m->from_node);
    mcast_node_multicast(n->state, m);                  // save the message in multicast
    amc_state_add(n->amc_st, m->uid, m->to_groups_len); // initialize message state

    if (n->id == n->leader_id) {
      to_order_add(&n->to_order, n->k_p + 1, 0, m->uid, m->to_groups, m->to_groups_len, MCAST_SET_HARD);
      try_to_propose(n, 0);
    }
  } else
    gettimeofday(&(n->last_prop), NULL);

  // if the decision happens before the content has arrived
  if (n->content_pending) {
    n->content_pending = 0;
    try_to_adeliver(n);
  }
}

/*
 * Task 2: When a new message m of type SND-SOFT is r-delivered
 * from a group in m.dst for the first time, it must be added to
 * the state and to the pending set (to_order).
 *
 * when r-deliver(snd-soft, h, x, m)
 *   if \forall y : (sync-soft, h, y, m) \in ToOrder then
 *     ToOrder <- ToOrder U (sync-soft, h, x, m)
 */
static void
handle_snd_soft(struct mcast_peer* p, mcast_message* m, void* arg)
{
  struct evfamcast_node* n = arg;
  m_ts_t                 old_ts;
  mcast_message_type     type_after;

  if (!mcast_node_is_delivered(n->state, m->uid)) {
    mcast_log_debug("SND-SOFT: (uid, gid, nid, ts) = (%" PRIu64 ", %d, %d, %u)", m->uid, m->from_group, m->from_node,
                    m->timestamp);
    amc_state_update_fast(n->amc_st, m->uid, m->timestamp, m->from_group, m->type, m->to_groups_len, &old_ts,
                          &type_after);
    if (m->timestamp > old_ts) { // only updates if greater
      // buffer_add(&n->buffer, m->timestamp, old_ts, m->uid, type_after);
      old_ts = m->timestamp;
    }

    if (n->id == n->leader_id) {
      if (type_after == MCAST_SYNC_SOFT) {
        to_order_add(&n->to_order, n->k_p + 1, old_ts, m->uid, m->to_groups, m->to_groups_len, type_after);
        try_to_propose(n, 0);
      }
    } else
      gettimeofday(&(n->last_prop), NULL);

  } else {
    mcast_log_debug("Already a-delivered: SND-SOFT: (uid, gid, nid, ts) = (%" PRIu64 ", %d, %d, %u)", m->uid,
                    m->from_group, m->from_node, m->timestamp);
  }
}

/*
 * Task 3: When a new message m of type SND-HARD is r-delivered
 * from a group in m.dst for the first time, it must be added to
 * the state and to the pending set (to_order).
 *
 * when r-deliver(snd-hard, h, x, m)
 *   if \forall y : (sync-hard, h, y, m) \in ToOrder then
 *     ToOrder <- ToOrder U (sync-hard, h, x, m)
 */
static void
handle_snd_hard(struct mcast_peer* p, mcast_message* m, void* arg)
{
  struct evfamcast_node* n = arg;
  m_ts_t                 old_ts;
  mcast_message_type     type_after;

  if (!mcast_node_is_delivered(n->state, m->uid)) {
    mcast_log_debug("SND-HARD: (uid, gid, nid, ts) = (%" PRIu64 ", %d, %d, %u)", m->uid, m->from_group, m->from_node,
                    m->timestamp);
    amc_state_update_fast(n->amc_st, m->uid, m->timestamp, m->from_group, m->type, m->to_groups_len, &old_ts,
                          &type_after);
    if (m->timestamp > old_ts || type_after == MCAST_FINAL) { // only updates if greater or final
      buffer_add(&n->buffer, m->timestamp, old_ts, m->uid, type_after);
      old_ts = m->timestamp;
    }

    if (type_after == MCAST_FINAL) { // fast path!
      try_to_adeliver(n);
    } else if (n->id == n->leader_id &&
               type_after == MCAST_SYNC_HARD) { // snd-hard from all groups in m.dst and fast path failed!
      to_order_add(&n->to_order, n->k_p + 1, old_ts, m->uid, m->to_groups, m->to_groups_len, type_after);
      try_to_propose(n, 0);
    } else
      gettimeofday(&(n->last_prop), NULL);
  } else {
    mcast_log_debug("Already a-delivered: SND-HARD: (uid, gid, nid, ts) = (%" PRIu64 ", %d, %d, %u)", m->uid,
                    m->from_group, m->from_node, m->timestamp);
  }
}
/*
 * Task 4: When ToOrder \ Ordered <> empty, propose
 *
 * NB: Only the group leader will propose.
 * Special case: if a proposed value was not accepted.
 *
 * when ToOrder \ Ordered not empty
 *   Cs <- max(Ch, Cs)
 *   for each (set-hard, h, x, m) \in ToOrder \ Ordered IN ORDER do
 *     Cs <- Cs + 1
 *     if |m.dst > 1| then r-multicast (snd-soft, h, Cs, m) to m.dst
 *	 propose[kp](ToOrder \ Ordered)
 *	 kp <- kp + 1
 */
static void
try_to_propose(struct evfamcast_node* n, int timedout)
{
  int                i = 0, count = to_order_count(&n->to_order);
  size_t             size = 0;
  m_ts_t             ts_before;
  mcast_message_type type_after;
  char*              proposal = NULL;
  struct prop_t*     msgs = NULL;
  mcast_message      soft = {.type = MCAST_SND_SOFT,
                        .uid = 0,
                        .from_node = n->id,
                        .from_group = n->group_id,
                        .value.mcast_value_len = 0,
                        .value.mcast_value_val = NULL };

  gettimeofday(&(n->last_prop), NULL);
  if (count > 0 && (n->k_p - n->k_d <= MAX_PAXOS_INSTANCES)) {
    (n->k_p)++;
    count = to_order_get_all(&n->to_order, &msgs);
    n->c_s = (n->c_h > n->c_s) ? n->c_h : n->c_s;
    for (i = 0; i < count; i++) {
      // mcast_log_debug("propose[%u]: Uid %" PRIu64 ", ts %u : timestamps (SOFT, HARD) = (%u, %u)", msgs[i].uid,
      //                msgs[i].ts, n->k_p, n->c_s, n->c_h);
      if (msgs[i].type == MCAST_SET_HARD) {
        ++(n->c_s);
        if (msgs[i].dst_size > 1) {
          soft.to_groups_len = msgs[i].dst_size;
          soft.uid = msgs[i].uid;
          soft.timestamp = n->c_s;
          memcpy(soft.to_groups, msgs[i].dst, msgs[i].dst_size * sizeof(m_gid_t));
          mcast_log_debug("sending SND-SOFT(%" PRIu64 ", %u), k_p = %u", soft.uid, soft.timestamp, n->k_p);
          evmcast_node_submit(n->mc_node, &soft, 1);
          amc_state_update_fast(n->amc_st, soft.uid, soft.timestamp, soft.from_group, soft.type, soft.to_groups_len,
                                &ts_before, &type_after);
          if (soft.timestamp > ts_before)
            ts_before = soft.timestamp;
          if (type_after == MCAST_SYNC_SOFT) { // if snd-soft from all g' in m.dst, then append to proposal
            to_order_add(&n->to_order, n->k_p, ts_before, soft.uid, soft.to_groups, soft.to_groups_len, type_after);
            count++;
          }
        }
      } else {
        if (msgs[i].ts > n->c_s) {
          n->c_s = msgs[i].ts;
        }
      }
    }

    size = pending_push(&n->pending, &n->to_order, &proposal); // to get any new sync-soft
    mcast_consensus_propose(&n->consensus, proposal, size);
    to_order_init(&n->to_order);
    mcast_log_debug("propose[%u]: %d msgs (%zu bytes)", n->k_p, count, size);
    if (timedout)
      mcast_log_info("Proposal by timeout!");
  }
}

/*
 * Task 5: When new decision in my group,
 * update buffer and ordered sets accordingly.
 *
 * when decide_g[kd](Decided)
 * 	 for each (z, h, x, m) \in Decided \ Ordered in order do
 *     if z = set-hard then Ch <- Ch + 1
 *       if |m.dst| > 1 then
 *         B <-B U {(snd-hard, g, Ch, m)}
 *         r-multicast (snd-hard, g, Ch, m) to m.dst
 *       else
 *         B <- B U {(sync-hard, g, Ch, m)}
 *     if z = sync-soft then
 *       Ch <- max({Ch , x})
 *       B <- B U {(sync-soft, h, x, m)}
 *     if z = sync-hard then
 *       Ch <- max({Ch , x})
 *       B <- B \ {(snd-hard, h, x, m)}
 *       B <- B U {(sync-hard, h, x, m)}
 *     Ordered <- Ordered U (z, h, x, m)
 *     kd <- kd + 1
 */
static void
when_decided(unsigned k_d, char* value, size_t size, void* arg)
{
  struct evfamcast_node* n = arg;
  struct to_order_t*     pend_to = NULL;
  struct prop_t *        msgs = (struct prop_t*)value, *m;
  int                    msg_count = size / sizeof(struct prop_t), i;
  m_ts_t                 ts_before = 0;
  mcast_message_type     t_after;
  mcast_message          snd_hard_msg = {.type = MCAST_SND_HARD,
                                .uid = 0,
                                .from_group = n->group_id,
                                .from_node = n->id,
                                .timestamp = 0,
                                .to_groups_len = 0,
                                .to_groups = { 0 },
                                .value = {.mcast_value_len = 0, .mcast_value_val = NULL } };

  assert(size % sizeof(struct prop_t) == 0);
  n->k_d = k_d;

  if (n->id == n->leader_id) {
    pending_pop(&n->pending, &pend_to);
    if (pend_to) {
      if (to_order_cmp(pend_to, msgs, msg_count)) {
        mcast_log_info("DECIDED[%u]: MY PROPOSAL WAS NOT DECIDED! TRYING AGAIN...", k_d);
        for (i = 0; i < msg_count; i++)
          to_order_add(&n->to_order, n->k_p + 1, msgs[i].ts, msgs[i].uid, msgs[i].dst, msgs[i].dst_size, msgs[i].type);
      }
    }
  }

  for (i = 0; i < msg_count; i++) {
    m = &msgs[i];
    mcast_log_debug("DECIDED[%u]: m(type, ts, uid) = (%s, %u, %" PRIu64 ")", k_d,
                    mcast_message_get_type_as_string(m->type), m->ts, m->uid);

    if (mcast_node_is_delivered(n->state, m->uid)) {
      mcast_log_debug("DECIDED[%u]: Already decided/delivered!", k_d);
      continue;
    }

    switch (m->type) {
      case MCAST_SET_HARD:
        m->ts = ++(n->c_h);
        if (m->dst_size == 1) {
          m->type = MCAST_FINAL;
          if (mcast_config.avoid_convoy) {
            deliverable_push(&n->deliverable, m->ts, m->uid);
            continue;
          }
          amc_state_update_fast(n->amc_st, m->uid, m->ts, n->group_id, m->type, m->dst_size, &ts_before, &t_after);
          buffer_add(&n->buffer, m->ts, ts_before, m->uid, t_after);
        } else {
          m->type = MCAST_SND_HARD;
          snd_hard_msg.uid = m->uid;
          snd_hard_msg.timestamp = m->ts;
          snd_hard_msg.to_groups_len = m->dst_size;
          if (n->id == n->leader_id) {
            memcpy(snd_hard_msg.to_groups, m->dst, m->dst_size * sizeof(m_gid_t));
            evmcast_node_submit(n->mc_node, &snd_hard_msg, 1);
          }
          handle_snd_hard(NULL, &snd_hard_msg, n);
        }

        break;

      case MCAST_SYNC_SOFT:
        n->c_h = (n->c_h < m->ts) ? m->ts : n->c_h;
        amc_state_update_fast(n->amc_st, m->uid, m->ts, n->group_id, m->type, m->dst_size, &ts_before, &t_after);
        if (t_after == MCAST_FINAL)
          buffer_add(&n->buffer, m->ts, ts_before, m->uid, t_after);
        else if (n->id == n->leader_id &&
                 t_after == MCAST_SYNC_HARD) { // snd-hard from all groups in m.dst and fast path failed!
          to_order_add(&n->to_order, n->k_p + 1, ts_before, m->uid, m->dst, m->dst_size, t_after);
          try_to_propose(n, 0);
        }
        break;

      case MCAST_SYNC_HARD:
        n->c_h = (n->c_h < m->ts) ? m->ts : n->c_h;
        m->type = MCAST_FINAL;
        amc_state_update_fast(n->amc_st, m->uid, m->ts, n->group_id, m->type, m->dst_size, &ts_before, &t_after);
        buffer_add(&n->buffer, m->ts, ts_before, m->uid, t_after);
        break;

      default:
        mcast_log_error("Invalid type delivered.");
        assert(0);
    }
  }

  try_to_adeliver(n);
  if (n->id == n->leader_id && (n->k_p - n->k_d == MAX_PAXOS_INSTANCES)) {
    mcast_log_debug("TRYING TO PROPOSE BECAUSE ALL INSTANCES WERE BUSY");
    try_to_propose(n, 0);
  }
}

/*
 * Task 6: Check if 'guess' was correct: if a SYNC-SOFT has the same timestamp of
 * a SYNC-HARD for the same group and message.
 *
 * when \exists h, x, m : (sync-soft, h, x, m) \in B ^ (sync-hard, h, x, m) \in (ToOrder \ Ordered)
 *   ts <- max({x : (sync-hard, h, x, m) \in B})
 *   B <- B U {(sync-hard,h,x,m)}
 *   Ordered <- Ordered U (sync-hard, h, x, m)
 */
// EXECUTED INSIDE amcast_structs

/*
 * Task 7:
 * - Part 1: convert possible messages to FINAL: done upon insertion of msgs to buffer
 * - Part 2: try to a-deliver as many FINAL messages as possible.
 *
 *
 * when \exists m, \forall h \in m.dst \exists x : (sync-hard, h, x, m) \in B
 *   ts <- max({x : (sync-hard, h, x, m) \in B})
 *   for each z,h,x : (z,h,x,m) \in B do B <- B \ {(z,h,x,m)}
 *   B <- B U {(final,?,ts,m)}
 *   while \exists (final,?,ts,m) \in B  \forall (z,h,x,m') \in B, m != m' : ts < x do
 *     B <- B \ {(final, ?, ts, m)}
 *     a-deliver m
 */
static void
try_to_adeliver(struct evfamcast_node* n)
{
  mcast_message start;
  m_ts_t        ts;
  m_uid_t       uid;

  while (buffer_get_front(&n->buffer, &ts, &uid, NULL, NULL)) {
    mcast_log_debug("ADDING TO DELIVERABLE: m(uid, ts) = (%" PRIu64 ", %u)", uid, ts);
    deliverable_push(&n->deliverable, ts, uid);
  }

  while (deliverable_pop(&n->deliverable, &ts, &uid, 0)) {
    if (!mcast_node_get_multicast(n->state, uid, &start)) {
      mcast_log_debug("MESSAGE (uid, ts) = (%" PRIu64 ", %u) CAN'T BE A-DELIVERED: CONTENT NOT RECEIVED YET!", uid, ts);
      n->content_pending = uid;
      break;
    }

    start.type = MCAST_DELIVERED;
    start.timestamp = ts;
    mcast_log_debug("A-DELIVERY: m(uid, ts) = (%" PRIu64 ", %u)", start.uid, start.timestamp);

    // remove from STATE and DELIVERABLE
    amc_state_del(n->amc_st, start.uid);
    deliverable_pop(&n->deliverable, &ts, &uid, 1);
    // deliver the message and free its contents
    evmcast_node_deliver(n->mc_node, &start);
    mcast_message_content_free(&start);
  }
}

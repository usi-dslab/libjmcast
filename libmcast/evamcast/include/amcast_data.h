/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _AMCAST_DATA_H
#define _AMCAST_DATA_H

#include "khash.h"
#include "mcast.h"

/**
 * @file Data structures to handle atomic multicast metadata
 *
 * @brief Implementation of ToOrder, Ordered, Pending and Buffer sets
 */

/**
 * Function to compare conflicting messages, when the semantic of the message needs to be taken into account.
 */
typedef int (*conflict_check_function_inner)(void* data_src, m_uid_t left_msg, m_uid_t right_msg);

struct msg_array_t;
struct msg_status_t;
struct pending_t;
struct ll_msg_t;
struct deliverable_t;

KHASH_MAP_INIT_INT64(msg, struct msg_status_t*)

typedef struct msg_array_t   to_order_t;
typedef struct pending_t     pending_t;
typedef kh_msg_t             ordered_t;
typedef struct ll_msg_t      buff_t;
typedef struct deliverable_t deliverable_t;

/**
 * Creates a new TO_ORDER set (array)
 * @param initial_size the initial size
 * @return the created set
 */
to_order_t* to_order_new(int initial_size);
/**
 * Frees the memory associated with this set
 * @param s the set
 */
void to_order_free(to_order_t* s);
/**
 * Apends a new message to the set
 * @param s TO_ORDER set
 * @param m message to be appended
 */
void to_order_add(to_order_t* s, mcast_message* m);
/**
 * Removes all messages from the set
 * @param s TO_ORDER set
 */
void to_order_empty(to_order_t* s);
/**
 * Allows getting the inner array of the set
 * @param s TO_ORDER set
 * @param m pointer to the array of messages
 * @return the size of the array
 */
int to_order_get_all(to_order_t* s, mcast_message** m);
/**
 * Number of messages in the set
 * @param s TO_ORDER set
 * @return the messages count
 */
int to_order_count(to_order_t* s);
/**
 * Creates a new PENDING set
 * @param initial_size the initial size each proposed instance
 * @param parallel_instances the configured number of instances allowed to run in parallel
 * @return the created set
 */
pending_t* pending_new(int initial_size, int parallel_instances);
/**
 * Frees the memory associated with the set
 * @param s PENDING set
 */
void pending_free(pending_t* s);
/**
 * Adds a new proposal to the set
 * @param s PENDING set
 * @param k_p the unique identifier of the current proposal
 * @param m the messages being proposed
 * @param size the messages count
 */
void pending_add(pending_t* s, unsigned k_p, mcast_message* m, int size);
/**
 * Gets the messages array associated with the proposal
 * @param s PENDING set
 * @param k_p the unique identifier of the current proposal
 * @param m the messages array
 * @return the size of messages array
 */
int pending_get(pending_t* s, unsigned k_p, mcast_message** m);
/**
 * Removes all messages with the specified proposal unique identifier
 * @param s PENDING set
 * @param k_p the unique identifier of the proposal
 */
void pending_del(pending_t* s, unsigned k_p);

/**
 * Creates a new ORDERED set
 * @return the created set
 */
ordered_t* ordered_new();
/**
 * Frees the memory associated with the set
 * @param s ORDERED set
 */
void ordered_free(ordered_t* s);
/**
 * Adds a new message to the set
 * @param s ORDERED set
 * @param m the message to be added
 */
void ordered_add(ordered_t* s, mcast_message* m);
/**
 * Removes all messages with the specified identifier from the set
 * @param s ORDERED set
 * @param uid the unique identifier of the message
 */
void ordered_del(ordered_t* s, m_uid_t uid);
/**
 * Searchs for the message in the set
 * @param s ORDERED set
 * @param t the type of the message
 * @param uid the unique identifier of the message
 * @param group the group the message comes from
 * @return 1 if found, 0 otherwise
 */
int ordered_has(ordered_t* s, mcast_message_type t, m_uid_t uid, m_gid_t group);
/**
 * Calculates the size of the set (with respect to unique identifiers)
 * @param s ORDERED set
 * @return messages count in the set
 */
int ordered_count(ordered_t* s);
/**
 * Creates a new BUFFER set
 * @return the created set
 */
buff_t* buffer_new();
/**
 * Frees the memory associated with the set
 * @param s BUFFER set
 */
void buffer_free(buff_t* s);
/**
 * Adds a new message to the set
 * @param s BUFFER set
 * @param m the message to be added
 */
void buffer_add(buff_t* s, mcast_message* m);
/**
 * Returns the next message in the set
 * @param s BUFFER set
 * @param m the message to be filled from the set
 * @param f the function to compare messages if messages
 *          should be reordered based on their contents
 * @param arg the data source to be passed to the comparing function
 * @return 1 if success, 0 if set is empty or an error occured
 */
int buffer_get_next(buff_t* s, mcast_message* m, conflict_check_function_inner f, void* arg);
/**
 * Removes all messages with the specified identifier from the set
 * @param s BUFFER set
 * @param uid the unique identifier of the message
 */
void buffer_del(buff_t* s, m_uid_t uid);
/**
 * Calculates the size of the set (with respect to unique identifiers)
 * @param s BUFFER set
 * @return messages count in the set
 */
int buffer_count(buff_t* s);
/**
 * Prints all messages in the set
 * @param s BUFFER set
 */
void buffer_print(buff_t* s);

/**
 * Creates a new DELIVERABLE set
 * @param initial_size the initial size of the set
 * @return the created set
 */
deliverable_t* deliverable_new(int initial_size);
/**
 * Freesthe memory associated with the set
 * @param s DELIVERABLE set
 */
void deliverable_free(deliverable_t* s);
/**
 * Adds a new message ready for delivery
 * @param s DELIVERABLE set
 * @param m the message
 */
void deliverable_push_back(deliverable_t* s, mcast_message* m);
/**
 * Pops the next message to be delivered
 * @param s DELIVERABLE set
 * @param m the message to be filled from the set
 * @return 1 if success, 0 if set is empty or an error occured
 */
int deliverable_pop_front(deliverable_t* s, mcast_message* m);
/**
 * Retrieves the next message to be delivered (but keeps it in the set)
 * @param s DELIVERABLE set
 * @param m the message to be filled from the set
 * @return 1 if success, 0 if set is empty or an error occured
 */
int deliverable_check_front(deliverable_t* s, mcast_message* m);
/**
 * Calculates the size of the set
 * @param s DELIVERABLE set
 * @return messages unique identifier count in the set
 */
int deliverable_count(deliverable_t* s);

#endif //_AMCAST_DATA_H

/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _AMCAST_STRUCTS_H
#define _AMCAST_STRUCTS_H

#include "khash.h"
#include "mcast.h"

#define MAX_AMC_STATE_SIZE 10000
#define MAX_PAXOS_INSTANCES 32
#define PENDING_SIZE 64 // always greater than MAX_PAXOS_INSTANCES

struct prop_t
{
  m_ts_t             ts;
  m_uid_t            uid;
  mcast_message_type type;
  size_t             dst_size;
  m_gid_t            dst[MAX_N_OF_GROUPS];
};

struct state_t
{
  m_uid_t            uid;
  m_ts_t             ts;
  mcast_message_type type;
  size_t             dst_size; // total nr of dst groups
  size_t             dst_count;
  m_ts_t             tss[MAX_N_OF_GROUPS];
  // FAST MULTICAST STUFFS!
  m_ts_t             ts_soft;
  size_t             dst_soft_count;
  m_ts_t             tss_soft[MAX_N_OF_GROUPS];
  mcast_message_type type_soft;
};

struct to_order_t
{
  size_t        instance;
  size_t        count;
  struct prop_t msgs[MAX_AMC_STATE_SIZE];
};

struct pending_t
{
  struct to_order_t pends[PENDING_SIZE];
  size_t            count;
  size_t            head;
  size_t            tail;
};

struct deliverable_t
{
  struct prop_t msgs[MAX_AMC_STATE_SIZE];
  size_t        count;
  size_t        head;
  size_t        tail;
};

struct buffer_t
{
  struct prop_t msgs[MAX_AMC_STATE_SIZE];
  size_t        count;
};

KHASH_MAP_INIT_INT64(state, struct state_t*)
typedef kh_state_t amc_state_t;

/**
 * Function to compare conflicting messages, when the semantic of the message needs to be taken into account.
 */
typedef int (*conflict_check_function_inner)(void* data_src, m_uid_t left_msg, m_uid_t right_msg);

/*
 * TO_ORDER
 */
void to_order_init(struct to_order_t* to);
void to_order_add(struct to_order_t* to, size_t instance, m_ts_t ts, m_uid_t uid, m_gid_t* dst, size_t dst_size,
                  mcast_message_type type);
int to_order_get_all(struct to_order_t* to, struct prop_t** p);
int to_order_count(struct to_order_t* to);
int to_order_cmp(struct to_order_t* to, struct prop_t* msgs, size_t count);
/*
 * PENDING
 */
void pending_init(struct pending_t* p);
size_t pending_push(struct pending_t* p, struct to_order_t* to, char** proposal);
int pending_pop(struct pending_t* p, struct to_order_t** to);
/*
 * BUFFER
 */
void buffer_init(struct buffer_t* b);
void buffer_add(struct buffer_t* b, m_ts_t new_ts, m_ts_t old_ts, m_uid_t uid, mcast_message_type type);
void buffer_del(struct buffer_t* b, m_ts_t ts, m_uid_t uid);
int buffer_get_front(struct buffer_t* b, m_ts_t* ts, m_uid_t* uid, conflict_check_function_inner ccf, void *data_src);
/*
 * DELIVERABLE
 */
void deliverable_init(struct deliverable_t* d);
void deliverable_push(struct deliverable_t* d, m_ts_t ts, m_uid_t uid);
int deliverable_pop(struct deliverable_t* d, m_ts_t* ts, m_uid_t* uid, int del);
/*
 * AMC_STATE
 */
amc_state_t* amc_state_new();
void amc_state_free(amc_state_t* st);
void amc_state_add(amc_state_t* st, m_uid_t uid, size_t dst_size);
void amc_state_del(amc_state_t* st, m_uid_t uid);
void amc_state_update(amc_state_t* st, m_uid_t uid, m_ts_t ts, m_gid_t gid, mcast_message_type type, size_t dst_size,
                      m_ts_t* ts_before, mcast_message_type* type_after);
void amc_state_update_fast(amc_state_t* st, m_uid_t uid, m_ts_t ts, m_gid_t gid, mcast_message_type type,
                           size_t dst_size, m_ts_t* ts_before, mcast_message_type* type_after);

#endif //_AMCAST_STRUCTS_H

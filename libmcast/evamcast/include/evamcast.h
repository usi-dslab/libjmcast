/*
 * Copyright (c) 2015-2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _EVAMCAST_H_
#define _EVAMCAST_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "evmcast.h"

struct evamcast_node;

typedef int (*conflict_check_function)(char* lmsg, int lmsg_size, char* rmsg, int rmsg_size);

struct evamcast_node* evamcast_node_init(int group_id, int id, const char* mcast_config, const char* paxos_config,
                                         struct event_base* base, mcast_deliver_function cb, void* arg,
                                         conflict_check_function ccf);
void evamcast_node_free(struct evamcast_node* n);
int evamcast_node_count_own_group(struct evamcast_node* n);

/* TODO: heartbeat-related functions: for leader election */
typedef void (*heartbeat_cb)(struct evamcast_node* n, int from_node, int flag_timeout, void* arg);
void mcast_send_heartbeat(struct evamcast_node* n, int ms_interval);
void mcast_heartbeat_callback(struct evamcast_node* n, heartbeat_cb hbcb, int ms_timeout);
void mcast_set_leader(struct evamcast_node* n, int leader_id);

/*  */

#ifdef __cplusplus
}
#endif
#endif

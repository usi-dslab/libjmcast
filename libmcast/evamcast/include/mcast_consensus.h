/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MCAST_CONSENSUS_H_
#define _MCAST_CONSENSUS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "mcast.h"
#include <event2/event.h>

typedef void (*ondecide_callback)(unsigned consensus_instance, char* value, size_t size, void* cb_arg);

struct mcast_consensus
{
  void* handle;
  struct
  {
    int (*connect_to_proposer)(void* handle, int proposer_id, const char* config_file);
    void (*propose)(void* handle, char* value, size_t size);
    void (*trim)(void* handle, unsigned consensus_instance);
    int (*start_learner)(void* handle, const char* config_file, ondecide_callback cb, void* cb_arg);
    void (*close)(void* handle);
  } api;
};

int mcast_consensus_init(struct mcast_consensus* c, struct event_base* base);
int mcast_consensus_connect_to_proposer(struct mcast_consensus* c, int proposer_id, const char* config_file);
void mcast_consensus_propose(struct mcast_consensus* c, char* value, size_t size);
void mcast_consensus_trim(struct mcast_consensus* c, unsigned consensus_instance);
int mcast_consensus_start_learner(struct mcast_consensus* c, const char* config_file, ondecide_callback cb,
                                  void* cb_arb);
void mcast_consensus_close(struct mcast_consensus* c);

/* each consensus library available should provide its 'init' function here */
int mcast_consensus_init_libpaxos(struct mcast_consensus* c, struct event_base* base);

#ifdef __cplusplus
}
#endif

#endif

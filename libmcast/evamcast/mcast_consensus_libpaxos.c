/*
 * Copyright (c) 2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast.h"
#include "mcast_consensus.h"
#include <errno.h>
#include <event2/bufferevent.h>
#include <evpaxos.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <string.h>

struct libpaxos_state
{
  int                 proposer_id;
  const char*         config_file;
  struct event_base*  base;
  struct bufferevent* prop_bev;
  struct evlearner*   learner;
  ondecide_callback   cb;
  void*               cb_arg;
};

static void on_connect(struct bufferevent* bev, short events, void* arg);
static struct libpaxos_state* libpaxos_state_new(struct event_base* base);
static void libpaxos_propose(void* handle, char* value, size_t size);
static int libpaxos_connect_to_proposer(void* handle, int proposer_id, const char* config_file);
static void libpaxos_trim(void* handle, unsigned consensus_instance);
static int libpaxos_start_learner(void* handle, const char* config_file, ondecide_callback cb, void* cb_arg);
static void libpaxos_close(void* handle);

int
mcast_consensus_init_libpaxos(struct mcast_consensus* c, struct event_base* base)
{
  c->handle = libpaxos_state_new(base);
  c->api.connect_to_proposer = libpaxos_connect_to_proposer;
  c->api.propose = libpaxos_propose;
  c->api.trim = libpaxos_trim;
  c->api.start_learner = libpaxos_start_learner;
  c->api.close = libpaxos_close;
  return (base != NULL && c->handle != NULL);
}

/* STATIC FUNCTIONS */
static struct libpaxos_state*
libpaxos_state_new(struct event_base* base)
{
  struct libpaxos_state* ls = malloc(sizeof(struct libpaxos_state));

  ls->prop_bev = NULL;
  ls->learner = NULL;
  ls->cb_arg = NULL;
  ls->config_file = NULL;
  ls->base = base;
  return ls;
}

static int
libpaxos_start_learner(void* handle, const char* config_file, ondecide_callback cb, void* cb_arg)
{
  struct libpaxos_state* ls = handle;

  if (!ls->learner)
    ls->learner = evlearner_init(config_file, cb, cb_arg, ls->base);

  return (ls->learner != NULL);
}

static void
libpaxos_close(void* handle)
{
  struct libpaxos_state* ls = handle;

  if (ls->learner)
    evlearner_free(ls->learner);

  if (ls->prop_bev)
    bufferevent_free(ls->prop_bev);

  free(ls);
}

static int
libpaxos_connect_to_proposer(void* handle, int proposer_id, const char* config_file)
{
  struct libpaxos_state* ls = handle;
  struct evpaxos_config* conf;
  struct sockaddr_in     addr;
  int                    flag = 1;

  if (!ls->prop_bev) {
    mcast_log_info("Connecting to proposer...");
    conf = evpaxos_config_read(config_file);
    if (conf == NULL) {
      mcast_log_error("Failed to read config file %s\n", config_file);
      return 0;
    }

    addr = evpaxos_proposer_address(conf, proposer_id);
    ls->proposer_id = proposer_id;
    ls->config_file = config_file;
    ls->prop_bev = bufferevent_socket_new(ls->base, -1, BEV_OPT_CLOSE_ON_FREE);
    bufferevent_setcb(ls->prop_bev, NULL, NULL, on_connect, ls);
    bufferevent_enable(ls->prop_bev, EV_READ | EV_WRITE);
    bufferevent_socket_connect(ls->prop_bev, (struct sockaddr*)&addr, sizeof(addr));
    setsockopt(bufferevent_getfd(ls->prop_bev), IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
    evpaxos_config_free(conf);
    return 1;
  }
  return 0;
}

static void
on_connect(struct bufferevent* bev, short events, void* arg)
{
  struct libpaxos_state* ls = arg;
  if (events & BEV_EVENT_CONNECTED) {
    mcast_log_info("...Connected to proposer");
  } else {
    mcast_log_error("%s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
    if (ls->prop_bev != NULL) {
      bufferevent_free(ls->prop_bev);
      ls->prop_bev = NULL;
    }
    libpaxos_connect_to_proposer(ls, ls->proposer_id, ls->config_file);
  }
}

static void
libpaxos_propose(void* handle, char* value, size_t size)
{
  struct libpaxos_state* ls = handle;

  if (ls->prop_bev == NULL) {
    mcast_log_error("Not connected to the proposer");
  }
  paxos_submit(ls->prop_bev, value, size);
}

static void
libpaxos_trim(void* handle, unsigned consensus_instance)
{
  struct libpaxos_state* ls = handle;

  evlearner_send_trim(ls->learner, consensus_instance);
}

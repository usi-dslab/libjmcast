/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _EVMCAST_H_
#define _EVMCAST_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "config_mcast.h"
#include "mcast.h"
#include "message_mcast.h"
#include <event2/bufferevent.h>
#include <event2/event.h>
#include <sys/types.h>

/* SERVER-SIDE FUNCTIONS */
struct evmcast_node;

/**
 * Callback function invoked upon message delivery
 */
typedef void (*mcast_deliver_function)(struct bufferevent* client, mcast_message* m, void* arg);

struct evmcast_node* evmcast_node_init(int group_id, int id, const char* config, struct event_base* base,
                                       mcast_deliver_function cb, void* arg);
void evmcast_node_free(struct evmcast_node* n);
void evmcast_node_submit(struct evmcast_node* n, mcast_message* m, int skip_own_group);
int evmcast_node_count_own_group(struct evmcast_node* n);

/* CLIENT-SIDE FUNCTIONS */
struct mcast_client;

typedef void (*mcast_client_connect_cb)(struct mcast_client* c, void* cb_arg);
typedef void (*mcast_client_reply_cb)(struct mcast_client* c, char* resp, int resp_len, void* cb_arg);

/**
 * Connects to a node identified by the provided group and node ids
 * @param group_id the group id of the node
 * @param id id of the node inside its group
 * @param config configuration file with information regarding the connection to the node
 * @param onread_cb function to be called when a value is received
 * @param onconnect_cb function to be called when a connection is established
 *
 * @return a mcast_client pointer representing the connection, which can be
 * further used to submit values to the node
 */
struct mcast_client* mcast_client_connect(int group_id, int id, const char* config, struct event_base* base,
                                          mcast_client_connect_cb ccb, mcast_client_reply_cb rcb, void* cb_arg);

/**
 * Clients should use this function to submit their commands
 * @param bev the bufferevent to deal with the data sent/received
 * @param data the content to be multicast
 * @param data_len the size of the content to be multicast
 * @param dst an array with the destination groups ids
 * @param dst_len the size of the destination array
 *
 * @see mcast_types.h
 */
void mcast_client_submit(struct mcast_client* c, char* data, int data_len, m_gid_t* dst, int dst_len);
void mcast_client_free(struct mcast_client* c);
m_uid_t mcast_client_get_uid(struct mcast_client* c);
int mcast_client_get_group_count(struct mcast_client* c);

/**
 * Special client which receives a copy of every delivered message
 * @param bev the replica endpoint connection
 * @param group the group the replica belongs to
 * @param id id of the replica inside the group
 */
void mcast_register_replica(struct bufferevent* bev, int group, int id);

#ifdef __cplusplus
}
#endif
#endif

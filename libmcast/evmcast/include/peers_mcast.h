/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _PEERS_MCAST_H_
#define _PEERS_MCAST_H_

#include "evmcast.h"
#include "mcast.h"
#include "mcast_types.h"
#include <event2/bufferevent.h>

#define MAX_SUBSCRIBE_TYPES 255

struct mcast_peer;
struct mcast_peers;

typedef void (*mcast_peer_cb)(struct mcast_peer* p, mcast_message* m, void* arg);

typedef void (*mcast_peer_iter_cb)(struct mcast_peer* p, void* arg);

struct mcast_peers* mcast_peers_new(struct event_base* base, struct evmcast_config* config);

void mcast_peers_free(struct mcast_peers* p);

int mcast_peers_count(struct mcast_peers* p);

void mcast_peers_connect_to_nodes(struct mcast_peers* p);

int mcast_peers_listen(struct mcast_peers* p, int port);

void mcast_peers_subscribe(struct mcast_peers* p, mcast_message_type t, mcast_peer_cb cb, void*);

void mcast_peers_foreach_node(struct mcast_peers* p, mcast_peer_iter_cb cb, void* arg);

void mcast_peers_foreach_node_in_group(struct mcast_peers* p, int group_id, mcast_peer_iter_cb cb, void* arg);

void mcast_peers_foreach_client(struct mcast_peers* p, mcast_peer_iter_cb cb, void* arg);

void mcast_peers_foreach_replica(struct mcast_peers* p, mcast_peer_iter_cb cb, void* arg);

struct mcast_peer* mcast_peers_get_node(struct mcast_peers* p, int group_id, int id);

struct event_base* mcast_peers_get_event_base(struct mcast_peers* p);

int mcast_peer_get_id(struct mcast_peer* p);

int mcast_peer_get_group_id(struct mcast_peer* p);

int mcast_peer_get_status(struct mcast_peer* p);

struct bufferevent* mcast_peer_get_buffer(struct mcast_peer* p);

int mcast_peer_connected(struct mcast_peer* p);

void mcast_peer_set_as_replica(struct mcast_peer* p, int group, int id);

void mcast_peers_set_sender(struct mcast_peers* p, m_uid_t uid, struct mcast_peer* peer);

struct mcast_peer* mcast_peers_get_sender(struct mcast_peers* p, m_uid_t uid, int unset_sender);

void mcast_peers_unset_sender(struct mcast_peers* p, m_uid_t uid);

#endif

/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "evmcast.h"
#include "message_mcast.h"
#include <arpa/inet.h>
#include <errno.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <inttypes.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct mcast_client
{
  m_uid_t                 uid;
  int                     group_count;
  struct bufferevent*     bev;
  mcast_client_connect_cb ccb;
  mcast_client_reply_cb   rcb;
  void*                   cb_arg;
};

static void on_client_connect(struct bufferevent* bev, short events, void* arg);
static void on_client_read(struct bufferevent* bev, void* arg);
static void socket_set_nodelay(int fd);

struct mcast_client*
mcast_client_connect(int group_id, int id, const char* config, struct event_base* base, mcast_client_connect_cb ccb,
                     mcast_client_reply_cb rcb, void* cb_arg)
{
  struct sockaddr_in     addr;
  struct evmcast_config* conf = evmcast_config_read(config);
  struct mcast_client*   c = NULL;
  static int             first_time = 1;
  struct timespec        nanos;

  if (first_time) {
    clock_gettime(CLOCK_MONOTONIC, &nanos);
    srandom(nanos.tv_nsec);
    first_time = 0;
  }

  if (conf == NULL) {
    mcast_log_error("Failed to read configuration file %s\n", config);
    return NULL;
  }

  c = malloc(sizeof(struct mcast_client));
  c->bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
  c->uid = random() % 65536;
  c->ccb = ccb;
  c->rcb = rcb;
  c->cb_arg = cb_arg;
  c->group_count = evmcast_group_count(conf);
  addr = evmcast_node_address(conf, group_id, id);
  bufferevent_setcb(c->bev, on_client_read, NULL, on_client_connect, c);
  evbuffer_enable_locking(bufferevent_get_input(c->bev), NULL);
  evbuffer_enable_locking(bufferevent_get_output(c->bev), NULL);
  bufferevent_enable(c->bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(c->bev, (struct sockaddr*)&addr, sizeof(addr));
  socket_set_nodelay(bufferevent_getfd(c->bev));
  evmcast_config_free(conf);
  return c;
}

void
mcast_client_submit(struct mcast_client* c, char* data, int data_len, m_gid_t* dst, int dst_len)
{
  mcast_message m;
  memset(&m, 0, sizeof(mcast_message));
  m.value.mcast_value_len = data_len;
  m.value.mcast_value_val = data;
  m.type = MCAST_CLIENT;
  m.to_groups_len = dst_len;
  memcpy(m.to_groups, dst, sizeof(m_gid_t) * dst_len);
  send_mcast_message(c->bev, &m);
}

void
mcast_client_free(struct mcast_client* c)
{
  bufferevent_free(c->bev);
  free(c);
}

m_uid_t
mcast_client_get_uid(struct mcast_client* c)
{
  return c->uid;
}

int
mcast_client_get_group_count(struct mcast_client* c)
{
  return c->group_count;
}

/* STATIC FUNCTIONS */
static void
socket_set_nodelay(int fd)
{
  int flag = mcast_config.tcp_nodelay;
  setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
}

static void
on_client_read(struct bufferevent* bev, void* arg)
{
  struct mcast_client* c = arg;
  mcast_message        msg;
  bufferevent_lock(bev);
  struct evbuffer*     in = bufferevent_get_input(bev);

  while (recv_mcast_message(in, &msg)) {
    c->rcb(c, msg.value.mcast_value_val, msg.value.mcast_value_len, c->cb_arg);
    mcast_message_content_free(&msg);
  }
  bufferevent_unlock(bev);
}

static void
on_client_connect(struct bufferevent* bev, short events, void* arg)
{
  struct mcast_client* c = arg;

  if (events & BEV_EVENT_CONNECTED) {
    mcast_log_info("Client %d: connected to node\n", c->uid);
    c->ccb(c, c->cb_arg);
  } else {
    mcast_log_error("Client %d: socket ERROR: %s\n", c->uid, evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
  }
}

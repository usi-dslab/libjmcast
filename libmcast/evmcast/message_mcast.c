/*
 * Copyright (c) 2015-2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "message_mcast.h"
#include "mcast.h"
#include "mcast_types_pack.h"
#include <assert.h>
#include <string.h>

static int
bufferevent_pack_mcast_message(void* data, const char* buf, size_t len)
{
  struct bufferevent* bev = (struct bufferevent*)data;
  bufferevent_write(bev, buf, len);
  return 0;
}

void
send_mcast_message(struct bufferevent* bev, mcast_message* msg)
{
  msgpack_packer packer;
  msgpack_packer_init(&packer, bev, bufferevent_pack_mcast_message);
  msgpack_pack_mcast_message(&packer, msg);
}

int
recv_mcast_message(struct evbuffer* in, mcast_message* out)
{
  int              rv = 0;
  char*            buffer;
  size_t           size, offset = 0;
  msgpack_unpacked msg;

  size = evbuffer_get_length(in);
  if (size == 0)
    return rv;

  msgpack_unpacked_init(&msg);
  buffer = (char*)evbuffer_pullup(in, size);
  if (msgpack_unpack_next(&msg, buffer, size, &offset)) {
    msgpack_unpack_mcast_message(&msg.data, out);
    evbuffer_drain(in, offset);
    rv = 1;
  }
  msgpack_unpacked_destroy(&msg);
  return rv;
}

void
mcast_register_replica(struct bufferevent* bev, int group, int id)
{
  mcast_message m;
  memset(&m, 0, sizeof(mcast_message));
  m.from_group = group;
  m.from_node = id;
  m.type = MCAST_REPLICA;
  send_mcast_message(bev, &m);
}

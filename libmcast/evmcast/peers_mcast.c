/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "peers_mcast.h"
#include "khash.h"
#include "message_mcast.h"
#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/listener.h>
#include <inttypes.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <string.h>

KHASH_MAP_INIT_INT64(client, struct mcast_peer*)

enum client_t
{
  CLIENT,
  REPLICA
};

struct mcast_peer
{
  int                 id;
  int                 group_id;
  int                 status;
  enum client_t       type;
  struct bufferevent* bev;
  struct event*       reconnect_ev;
  struct sockaddr_in  addr;
  struct mcast_peers* mcast_peers;
};

struct subscription
{
  mcast_message_type type;
  mcast_peer_cb      callback;
  void*              arg;
};

struct mcast_peers
{
  int                    mcast_peers_count;
  struct mcast_peer**    mcast_peers;
  int                    clients_count;
  struct mcast_peer**    clients;
  struct evconnlistener* listener;
  struct event_base*     base;
  struct evmcast_config* config;
  int                    subs_count;
  struct subscription    subs[MAX_SUBSCRIBE_TYPES];
  kh_client_t*           h_clients;
};

static struct timeval reconnect_timeout = { 2, 0 };

static struct mcast_peer* make_mcast_peer(struct mcast_peers* p, int group_id, int id, struct sockaddr_in* in);
static void free_mcast_peer(struct mcast_peer* p);
static void free_all_mcast_peers(struct mcast_peer** p, int count);
static void connect_mcast_peer(struct mcast_peer* p);
static void mcast_peers_connect(struct mcast_peers* p, int group_id, int id, struct sockaddr_in* addr);
static void on_read(struct bufferevent* bev, void* arg);
static void on_mcast_peer_event(struct bufferevent* bev, short ev, void* arg);
static void on_client_event(struct bufferevent* bev, short events, void* arg);
static void on_connection_timeout(int fd, short ev, void* arg);
static void on_listener_error(struct evconnlistener* l, void* arg);
static void on_accept(struct evconnlistener* l, evutil_socket_t fd, struct sockaddr* addr, int socklen, void* arg);
static void socket_set_nodelay(int fd);
static int mcast_peers_get_client_idx(struct mcast_peer* client);

struct mcast_peers*
mcast_peers_new(struct event_base* base, struct evmcast_config* config)
{
  struct mcast_peers* p = malloc(sizeof(struct mcast_peers));
  p->clients_count = 0;
  p->mcast_peers_count = 0;
  memset(p->subs, 0, MAX_SUBSCRIBE_TYPES * sizeof(struct subscription));
  p->subs_count = 0;
  p->mcast_peers = NULL;
  p->clients = NULL;
  p->listener = NULL;
  p->base = base;
  p->config = config;
  p->h_clients = kh_init(client);
  return p;
}

void
mcast_peers_free(struct mcast_peers* p)
{
  kh_destroy_client(p->h_clients);
  free_all_mcast_peers(p->mcast_peers, p->mcast_peers_count);
  free_all_mcast_peers(p->clients, p->clients_count);
  if (p->listener != NULL)
    evconnlistener_free(p->listener);
  free(p);
}

int
mcast_peers_count(struct mcast_peers* p)
{
  return p->mcast_peers_count;
}

static void
mcast_peers_connect(struct mcast_peers* p, int group_id, int id, struct sockaddr_in* addr)
{
  p->mcast_peers = realloc(p->mcast_peers, sizeof(struct mcast_peer*) * (p->mcast_peers_count + 1));
  p->mcast_peers[p->mcast_peers_count] = make_mcast_peer(p, group_id, id, addr);

  struct mcast_peer* mcast_peer = p->mcast_peers[p->mcast_peers_count];
  bufferevent_setcb(mcast_peer->bev, on_read, NULL, on_mcast_peer_event, mcast_peer);
  mcast_peer->reconnect_ev = evtimer_new(p->base, on_connection_timeout, mcast_peer);
  connect_mcast_peer(mcast_peer);

  p->mcast_peers_count++;
}

void
mcast_peers_connect_to_nodes(struct mcast_peers* p)
{
  int i, j;
  for (i = 0; i < evmcast_group_count(p->config); i++) {
    for (j = 0; j < evmcast_node_count(p->config, i); j++) {
      struct sockaddr_in addr = evmcast_node_address(p->config, i, j);
      mcast_peers_connect(p, i, j, &addr);
    }
  }
}

void
mcast_peers_foreach_node(struct mcast_peers* p, mcast_peer_iter_cb cb, void* arg)
{
  int i;
  for (i = 0; i < p->mcast_peers_count; ++i)
    cb(p->mcast_peers[i], arg);
}

void
mcast_peers_foreach_node_in_group(struct mcast_peers* p, int group_id, mcast_peer_iter_cb cb, void* arg)
{
  int i;
  for (i = 0; i < p->mcast_peers_count; ++i)
    if (p->mcast_peers[i]->group_id == group_id)
      cb(p->mcast_peers[i], arg);
}

void
mcast_peers_foreach_client(struct mcast_peers* p, mcast_peer_iter_cb cb, void* arg)
{
  int i;
  for (i = 0; i < p->clients_count; ++i)
    cb(p->clients[i], arg);
}

void
mcast_peers_foreach_replica(struct mcast_peers* p, mcast_peer_iter_cb cb, void* arg)
{
  int i;
  for (i = 0; i < p->clients_count; ++i)
    if (p->clients[i]->type == REPLICA)
      cb(p->clients[i], arg);
}

struct mcast_peer*
mcast_peers_get_node(struct mcast_peers* p, int group_id, int id)
{
  int i;
  for (i = 0; p->mcast_peers_count; ++i)
    if (p->mcast_peers[i]->group_id == group_id && p->mcast_peers[i]->id == id)
      return p->mcast_peers[i];
  return NULL;
}

struct bufferevent*
mcast_peer_get_buffer(struct mcast_peer* p)
{
  return p->bev;
}

int
mcast_peer_get_id(struct mcast_peer* p)
{
  return p->id;
}

int
mcast_peer_get_group_id(struct mcast_peer* p)
{
  return p->group_id;
}

int
mcast_peer_get_status(struct mcast_peer* p)
{
  return p->status;
}

int
mcast_peer_connected(struct mcast_peer* p)
{
  return p->status == BEV_EVENT_CONNECTED;
}

void
mcast_peer_set_as_replica(struct mcast_peer* p, int group, int id)
{
  p->group_id = group;
  p->id = id;
  p->type = REPLICA;
}

int
mcast_peers_listen(struct mcast_peers* p, int port)
{
  struct sockaddr_in addr;
  unsigned           flags = LEV_OPT_CLOSE_ON_EXEC | LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE;

  /* listen on the given port at address 0.0.0.0 */
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(0);
  addr.sin_port = htons(port);

  p->listener = evconnlistener_new_bind(p->base, on_accept, p, flags, -1, (struct sockaddr*)&addr, sizeof(addr));
  if (p->listener == NULL) {
    mcast_log_error("Failed to bind on port %d", port);
    return 0;
  }
  evconnlistener_set_error_cb(p->listener, on_listener_error);
  mcast_log_info("Listening on port %d", port);
  return 1;
}

void
mcast_peers_subscribe(struct mcast_peers* p, mcast_message_type type, mcast_peer_cb cb, void* arg)
{
  struct subscription* sub;

  assert((int)type < MAX_SUBSCRIBE_TYPES);
  sub = &p->subs[type];
  sub->type = type;
  sub->callback = cb;
  sub->arg = arg;
  p->subs_count++;
}

struct event_base*
mcast_peers_get_event_base(struct mcast_peers* p)
{
  return p->base;
}

void
mcast_peers_set_sender(struct mcast_peers* p, m_uid_t uid, struct mcast_peer* peer)
{
  int      rv;
  khiter_t k;

  if (peer->type == REPLICA) // don't need to add replica
    return;

  k = kh_put_client(p->h_clients, uid, &rv);
  if (rv == -1) {
    mcast_log_error("Error while setting sender for message with uid = %" PRIu64, uid);
    return;
  }
  kh_value(p->h_clients, k) = peer;
}

void
mcast_peers_unset_sender(struct mcast_peers* p, m_uid_t uid)
{
  khiter_t k;
  k = kh_get_client(p->h_clients, uid);
  if (k != kh_end(p->h_clients))
    kh_del_client(p->h_clients, k);
}

struct mcast_peer*
mcast_peers_get_sender(struct mcast_peers* p, m_uid_t uid, int unset_sender)
{
  khiter_t           k;
  struct mcast_peer* peer = NULL;
  k = kh_get_client(p->h_clients, uid);
  if (k != kh_end(p->h_clients)) {
    peer = kh_value(p->h_clients, k);
    if (unset_sender)
      kh_del_client(p->h_clients, k);
  }
  return peer;
}

/* STATIC FUNCTIONS */
static void
dispatch_message(struct mcast_peer* p, mcast_message* msg)
{
  struct subscription* s = p->mcast_peers->subs;
  if ((int)msg->type < MAX_SUBSCRIBE_TYPES && s[msg->type].type == msg->type)
    s[msg->type].callback(p, msg, s[msg->type].arg);
}

static void
on_read(struct bufferevent* bev, void* arg)
{
  mcast_message      msg;
  struct mcast_peer* p = (struct mcast_peer*)arg;
  struct evbuffer*   in = bufferevent_get_input(bev);
  while (recv_mcast_message(in, &msg)) {
    dispatch_message(p, &msg);
    mcast_message_content_free(&msg);
  }
}

static void
on_mcast_peer_event(struct bufferevent* bev, short ev, void* arg)
{
  struct mcast_peer* p = (struct mcast_peer*)arg;

  if (ev & BEV_EVENT_CONNECTED) {
    mcast_log_info("Connected to (gid/nid) = (%d/%d), address %s:%d", p->group_id, p->id, inet_ntoa(p->addr.sin_addr),
                   ntohs(p->addr.sin_port));
    p->status = ev;
  } else if (ev & BEV_EVENT_ERROR || ev & BEV_EVENT_EOF) {
    struct event_base* base;
    int                err = EVUTIL_SOCKET_ERROR();
    mcast_log_error("%s (gid/nid) = (%d/%d), address %s:%d", evutil_socket_error_to_string(err), p->group_id, p->id,
                    inet_ntoa(p->addr.sin_addr), ntohs(p->addr.sin_port));
    base = bufferevent_get_base(p->bev);
    bufferevent_free(p->bev);
    p->bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
    bufferevent_setcb(p->bev, on_read, NULL, on_mcast_peer_event, p);
    event_add(p->reconnect_ev, &reconnect_timeout);
    p->status = ev;
  } else {
    mcast_log_error("Event %d not handled", ev);
  }
}

static void
on_client_event(struct bufferevent* bev, short ev, void* arg)
{
  struct mcast_peer* p = (struct mcast_peer*)arg;
  if (ev & BEV_EVENT_EOF || ev & BEV_EVENT_ERROR) {
    int                 i, id;
    struct mcast_peer** clients = p->mcast_peers->clients;
    id = mcast_peers_get_client_idx(p);
    assert(id >= 0);
    for (i = id; i < p->mcast_peers->clients_count - 1; ++i) {
      clients[i] = clients[i + 1];
    }
    p->mcast_peers->clients_count--;
    p->mcast_peers->clients =
      realloc(p->mcast_peers->clients, sizeof(struct mcast_peer*) * (p->mcast_peers->clients_count));
    free_mcast_peer(p);
  } else {
    mcast_log_error("Event %d not handled", ev);
  }
}

static void
on_connection_timeout(int fd, short ev, void* arg)
{
  connect_mcast_peer((struct mcast_peer*)arg);
}

static void
on_listener_error(struct evconnlistener* l, void* arg)
{
  int                err = EVUTIL_SOCKET_ERROR();
  struct event_base* base = evconnlistener_get_base(l);
  mcast_log_error("Listener error %d: %s. Shutting down event loop.", err, evutil_socket_error_to_string(err));
  event_base_loopexit(base, NULL);
}

static void
on_accept(struct evconnlistener* l, evutil_socket_t fd, struct sockaddr* addr, int socklen, void* arg)
{
  struct mcast_peer*  mcast_peer;
  struct mcast_peers* mcast_peers = arg;
  int                 id = -1, group_id = -1;

  mcast_peers->clients = realloc(mcast_peers->clients, sizeof(struct mcast_peer*) * (mcast_peers->clients_count + 1));
  mcast_peers->clients[mcast_peers->clients_count] =
    make_mcast_peer(mcast_peers, group_id, id, (struct sockaddr_in*)addr);

  mcast_peer = mcast_peers->clients[mcast_peers->clients_count];
  evbuffer_enable_locking(bufferevent_get_input(mcast_peer->bev), NULL);
  evbuffer_enable_locking(bufferevent_get_output(mcast_peer->bev), NULL);
  bufferevent_setfd(mcast_peer->bev, fd);
  bufferevent_setcb(mcast_peer->bev, on_read, NULL, on_client_event, mcast_peer);
  bufferevent_enable(mcast_peer->bev, EV_READ | EV_WRITE);
  socket_set_nodelay(fd);

  mcast_log_info("Accepted connection from (gid/nid) = (%d/%d), address %s:%d", mcast_peer->group_id, mcast_peer->id,
                 inet_ntoa(((struct sockaddr_in*)addr)->sin_addr), ntohs(((struct sockaddr_in*)addr)->sin_port));

  mcast_peers->clients_count++;
}

static void
connect_mcast_peer(struct mcast_peer* p)
{
  bufferevent_enable(p->bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(p->bev, (struct sockaddr*)&p->addr, sizeof(p->addr));
  socket_set_nodelay(bufferevent_getfd(p->bev));
  mcast_log_info("Connect to (gid/nid) = (%d/%d), address %s:%d", p->group_id, p->id, inet_ntoa(p->addr.sin_addr),
                 ntohs(p->addr.sin_port));
}

static struct mcast_peer*
make_mcast_peer(struct mcast_peers* mcast_peers, int group_id, int id, struct sockaddr_in* addr)
{
  struct mcast_peer* p = malloc(sizeof(struct mcast_peer));
  p->group_id = group_id;
  p->id = id;
  p->type = CLIENT;
  p->addr = *addr;
  p->bev = bufferevent_socket_new(mcast_peers->base, -1, BEV_OPT_CLOSE_ON_FREE);
  p->mcast_peers = mcast_peers;
  p->reconnect_ev = NULL;
  p->status = BEV_EVENT_EOF;
  return p;
}

static void
free_all_mcast_peers(struct mcast_peer** p, int count)
{
  int i;
  for (i = 0; i < count; i++)
    free_mcast_peer(p[i]);
  if (count > 0)
    free(p);
}

static void
free_mcast_peer(struct mcast_peer* p)
{
  bufferevent_free(p->bev);
  if (p->reconnect_ev != NULL)
    event_free(p->reconnect_ev);
  free(p);
}

static void
socket_set_nodelay(int fd)
{
  int flag = mcast_config.tcp_nodelay;
  setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
}

static int
is_client(struct mcast_peers* mcast_peers, struct sockaddr_in* addr)
{
  int                 i;
  struct sockaddr_in* aux;
  for (i = 0; i < mcast_peers->clients_count; i++) {
    aux = &(mcast_peers->clients[i]->addr);
    if (aux->sin_addr.s_addr == addr->sin_addr.s_addr && aux->sin_port == addr->sin_port)
      return i;
  }
  return -1;
}

static int
mcast_peers_get_client_idx(struct mcast_peer* client)
{
  struct mcast_peers* p;
  struct sockaddr_in* addr;
  p = client->mcast_peers;
  addr = &(client->addr);
  return is_client(p, addr);
}

/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _LIBMCAST_H_
#define _LIBMCAST_H_

#include <mcast_types.h>
#include <stdarg.h>
#include <sys/types.h>

/* Logging and verbosity levels */
typedef enum { MCAST_LOG_QUIET = 0, MCAST_LOG_ERROR = 1, MCAST_LOG_INFO = 2, MCAST_LOG_DEBUG = 3 } mcast_log_level;

/* Configuration */
struct mcast_config
{
  /* General configuration */
  mcast_log_level verbosity;
  int             tcp_nodelay;
  int             message_timeout;
  int             batch_size;
  int             batch_max_interval;
  int             avoid_convoy;
  int             replica_mode;
  char*           paxos;
  /* lmdb storage configuration */
  int    persist;
  int    trash_files;
  int    lmdb_sync;
  char*  lmdb_env_path;
  size_t lmdb_mapsize;
};

extern struct mcast_config mcast_config;

/* Core functions */
mcast_message* mcast_message_new(mcast_message_type type, char* c, size_t len, uint16_t to_groups_len,
                                 m_gid_t* to_groups);

void mcast_message_free(mcast_message* m);

void mcast_message_content_set(mcast_message* m, char* c, size_t len);

void mcast_message_content_free(mcast_message* m);

mcast_value mcast_message_content_get(mcast_message* m);

mcast_message* mcast_message_clone(mcast_message* original, int need_clone_content);

void mcast_message_copy(mcast_message* dst, mcast_message* src, int need_clone_content);

const char* mcast_message_get_type_as_string(mcast_message_type type);

/* Logging */
void mcast_log(int level, const char* format, va_list ap);

void mcast_log_error(const char* format, ...);

void mcast_log_info(const char* format, ...);

void mcast_log_debug(const char* format, ...);

/* Uid management */
m_uid_t generate_uid(m_gid_t gid, m_nid_t nid, m_sn_t sn);

m_gid_t key_get_gid(m_uid_t uid);

m_nid_t key_get_nid(m_uid_t uid);

m_sn_t key_get_sn(m_uid_t uid);

#endif

/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MCAST_NODE_H_
#define _MCAST_NODE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "mcast.h"

/**
 * @file mcast_node.h
 * @brief mcast node represents the basic state of a node regard the multicast groups and messages exchange
 */

/**
 * @struct mcast_node
 * Represents a simple mcast node, with an id, group_id.
 * Holds the state of multicast and delivered messages.
 * @see mcast_node.c
 */
struct mcast_node;

/** Creates a new mcast node
 *
 * @param id        The id of this mcast node in its group
 * @param group_id  The group id
 * @return          The reference to the mcast node
 */
struct mcast_node* mcast_node_new(int id, int group_id);

/** Deletes a mcast node
 *
 * The mcast node and all in-memory information are freed.
 *
 * @param the mcast node to be freed
 */
void mcast_node_free(struct mcast_node* n);

int mcast_node_get_id(struct mcast_node* n);

int mcast_node_get_group_id(struct mcast_node* n);

m_uid_t mcast_node_set_uid(struct mcast_node* n, mcast_message* m);

int mcast_node_multicast(struct mcast_node* n, mcast_message* m);

int mcast_node_deliver(struct mcast_node* n, mcast_message* m);

int mcast_node_multicast_count(struct mcast_node* n);

int mcast_node_delivered_count(struct mcast_node* n);

int mcast_node_get_multicast(struct mcast_node* n, m_uid_t uid, mcast_message* ret);

int mcast_node_get_delivered(struct mcast_node* n, m_uid_t uid, mcast_message* ret);

int mcast_node_is_multicast(struct mcast_node* n, m_uid_t uid);

int mcast_node_is_delivered(struct mcast_node* n, m_uid_t uid);

int mcast_node_trim(struct mcast_node* n, m_ts_t ts);


#ifdef __cplusplus
}
#endif

#endif

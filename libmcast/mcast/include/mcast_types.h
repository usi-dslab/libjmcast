/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MCAST_TYPES_H_
#define _MCAST_TYPES_H_

#include <stdint.h>

#define MAX_N_OF_NODES 10
#define MAX_N_OF_GROUPS 10

typedef uint16_t m_gid_t;
typedef uint16_t m_nid_t;
typedef uint32_t m_sn_t;
typedef uint32_t m_ts_t;
typedef uint64_t m_uid_t;

struct mcast_value
{
  int   mcast_value_len;
  char* mcast_value_val;
};
typedef struct mcast_value mcast_value;

enum mcast_message_type
{
  MCAST_CLIENT = 1,
  MCAST_REPLICA = 2,
  MCAST_START = 3,
  MCAST_SND_HARD = 4,
  MCAST_SET_HARD = 5,
  MCAST_SYNC_HARD = 6,
  MCAST_SND_SOFT = 7,
  MCAST_SYNC_SOFT = 8,
  MCAST_FINAL = 9,
  MCAST_DELIVERED = 10,
  MCAST_HEARTBEAT = 11
};

typedef enum mcast_message_type mcast_message_type;

struct mcast_message
{
  mcast_message_type type;
  m_uid_t            uid;
  /* sequence number defined when the message is first multicast */
  m_gid_t from_group;
  m_nid_t from_node;
  m_ts_t  timestamp;
  /* timestamp decided (if ordering is implemented) or delivery order*/
  uint16_t    to_groups_len;
  m_gid_t     to_groups[MAX_N_OF_GROUPS];
  mcast_value value;
};
typedef struct mcast_message mcast_message;

#endif

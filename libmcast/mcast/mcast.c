/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

static const char* MTD[] = { "UNDEFINED", "CLIENT",   "REPLICA",   "START", "SND-HARD", "SET-HARD",
                             "SYNC-HARD", "SND-SOFT", "SYNC-SOFT", "FINAL", "DELIVERED" };

struct mcast_config mcast_config = {.verbosity = MCAST_LOG_ERROR,
                                    .tcp_nodelay = 1,
                                    .message_timeout = 100000,
                                    .batch_size = 1,
                                    .batch_max_interval = 5,
                                    .avoid_convoy = 1,
                                    .replica_mode = 0,
                                    .paxos = "libpaxos",
                                    .persist = 0,
                                    .trash_files = 0,
                                    .lmdb_sync = 0,
                                    .lmdb_env_path = "/tmp/node",
                                    .lmdb_mapsize = 100 * 1024 * 1024 };

/* Core functions */
mcast_message*
mcast_message_new(mcast_message_type type, char* c, size_t len, uint16_t to_groups_len, m_gid_t* to_groups)
{
  mcast_message* m;
  assert(to_groups_len > 0);
  m = malloc(sizeof(mcast_message));
  memset(m, 0, sizeof(mcast_message));
  m->type = type;
  m->to_groups_len = to_groups_len;
  memcpy(m->to_groups, to_groups, to_groups_len * sizeof(m_gid_t));
  if (len > 0 && c != NULL) {
    m->value.mcast_value_len = len;
    m->value.mcast_value_val = c;
  }
  return m;
}

void
mcast_message_free(mcast_message* m)
{
  mcast_message_content_free(m);
  free(m);
}

void
mcast_message_content_set(mcast_message* m, char* c, size_t len)
{
  if (m->value.mcast_value_val != NULL) {
    mcast_log_info("Replacing content of a message. Freeing old one...");
    mcast_message_content_free(m);
  }
  m->value.mcast_value_val = c;
  m->value.mcast_value_len = len;
}

mcast_value
mcast_message_content_get(mcast_message* m)
{
  return m->value;
}

void
mcast_message_content_free(mcast_message* m)
{
  if (m->value.mcast_value_val != NULL && m->value.mcast_value_len > 0) {
    free(m->value.mcast_value_val);
    m->value.mcast_value_len = 0;
    m->value.mcast_value_val = NULL;
  }
}

mcast_message*
mcast_message_clone(mcast_message* original, int need_clone_content)
{
  mcast_message* new;
  new = malloc(sizeof(mcast_message));
  memcpy(new, original, sizeof(mcast_message));
  if (need_clone_content && original->value.mcast_value_len > 0) {
    new->value.mcast_value_len = original->value.mcast_value_len;
    new->value.mcast_value_val = malloc(original->value.mcast_value_len);
    memcpy(new->value.mcast_value_val, original->value.mcast_value_val, original->value.mcast_value_len);
  } else {
    new->value.mcast_value_len = 0;
    new->value.mcast_value_val = NULL;
  }
  return new;
}

void
mcast_message_copy(mcast_message* dst, mcast_message* src, int need_clone_content)
{
  memcpy(dst, src, sizeof(mcast_message));
  if (need_clone_content && src->value.mcast_value_len > 0) {
    dst->value.mcast_value_len = src->value.mcast_value_len;
    dst->value.mcast_value_val = malloc(src->value.mcast_value_len);
    memcpy(dst->value.mcast_value_val, src->value.mcast_value_val, src->value.mcast_value_len);
  } else {
    dst->value.mcast_value_len = 0;
    dst->value.mcast_value_val = NULL;
  }
}

const char*
mcast_message_get_type_as_string(mcast_message_type type)
{
  return MTD[type];
}

void
mcast_log(int level, const char* format, va_list ap)
{
  int            off;
  char           msg[1024];
  struct timeval tv;

  if (level > mcast_config.verbosity)
    return;

  gettimeofday(&tv, NULL);
  off = strftime(msg, sizeof(msg), "%d %b %H:%M:%S. ", localtime(&tv.tv_sec));
  vsnprintf(msg + off, sizeof(msg) - off, format, ap);
  fprintf(stdout, "%s\n", msg);
}

void
mcast_log_error(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  mcast_log(MCAST_LOG_ERROR, format, ap);
  va_end(ap);
}

void
mcast_log_info(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  mcast_log(MCAST_LOG_INFO, format, ap);
  va_end(ap);
}

void
mcast_log_debug(const char* format, ...)
{
  va_list ap;
  va_start(ap, format);
  mcast_log(MCAST_LOG_DEBUG, format, ap);
  va_end(ap);
}

m_uid_t
generate_uid(m_gid_t gid, m_nid_t nid, m_sn_t sn)
{
  m_uid_t rv, _gid, _nid;
  _gid = gid;
  _nid = nid;
  rv = (_gid << 48) | (_nid << 32) | sn;
  return rv;
}

m_gid_t
key_get_gid(m_uid_t uid)
{
  return uid >> 48;
}

m_nid_t
key_get_nid(m_uid_t uid)
{
  return (uid >> 32) & 0xFFFF;
}

m_sn_t
key_get_sn(m_uid_t uid)
{
  return uid & 0xFFFFFFFF;
}

/*
 * Copyright (c) 2015-2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast.h"
#include "mcast_persistence.h"
#include <assert.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

struct mcast_node
{
  int                  id;
  int                  group_id;
  struct mcast_persist mcast;
  struct mcast_persist dlv;
  m_sn_t               next_seq_number;
};

static int persist(struct mcast_persist* p, mcast_message* m);
static int get_record(struct mcast_persist* p, m_uid_t uid, mcast_message* m);
static int trim(struct mcast_persist* p, m_ts_t ts);

struct mcast_node*
mcast_node_new(int id, int group_id)
{
  struct mcast_node* n;
  n = malloc(sizeof(struct mcast_node));
  n->id = id;
  n->group_id = group_id;
  n->next_seq_number = 0;
  mcast_persist_init(&n->dlv, (group_id * MAX_N_OF_NODES + id), "dlv");
  mcast_persist_init(&n->mcast, (group_id * MAX_N_OF_NODES + id), "mc");
  if (mcast_persist_open(&n->dlv) != 0 || mcast_persist_open(&n->mcast) != 0) {
    free(n);
    return NULL;
  }
  if (mcast_persist_tx_begin(&n->dlv) != 0 || mcast_persist_tx_begin(&n->mcast) != 0)
    return NULL;
  if (mcast_persist_tx_commit(&n->dlv) != 0 || mcast_persist_tx_commit(&n->mcast) != 0)
    return NULL;

  return n;
}

void
mcast_node_free(struct mcast_node* n)
{
  mcast_persist_close(&n->mcast);
  mcast_persist_close(&n->dlv);
  free(n);
}

m_uid_t
mcast_node_set_uid(struct mcast_node* n, mcast_message* m)
{
  assert(m != NULL);
  m->from_group = n->group_id;
  m->from_node = n->id;
  m->uid = generate_uid(n->group_id, n->id, ++(n->next_seq_number));
  return m->uid;
}

int
mcast_node_get_id(struct mcast_node* n)
{
  return n->id;
}

int
mcast_node_get_group_id(struct mcast_node* n)
{
  return n->group_id;
}

int
mcast_node_multicast(struct mcast_node* n, mcast_message* m)
{
  return persist(&n->mcast, m);
}

int
mcast_node_deliver(struct mcast_node* n, mcast_message* m)
{
  return persist(&n->dlv, m);
}

int
mcast_node_multicast_count(struct mcast_node* n)
{
  return mcast_persist_get_record_count(&n->mcast);
}

int
mcast_node_delivered_count(struct mcast_node* n)
{
  return mcast_persist_get_record_count(&n->dlv);
}

int
mcast_node_get_multicast(struct mcast_node* n, m_uid_t uid, mcast_message* ret)
{
  return get_record(&n->mcast, uid, ret);
}

int
mcast_node_get_delivered(struct mcast_node* n, m_uid_t uid, mcast_message* ret)
{
  return get_record(&n->dlv, uid, ret);
}

int
mcast_node_is_multicast(struct mcast_node* n, m_uid_t uid)
{
  return get_record(&n->mcast, uid, NULL);
}

int
mcast_node_is_delivered(struct mcast_node* n, m_uid_t uid)
{
  return get_record(&n->dlv, uid, NULL);
}

int
mcast_node_trim(struct mcast_node* n, m_ts_t ts)
{
  return trim(&n->dlv, ts) && trim(&n->mcast, ts);
}

static int
persist(struct mcast_persist* p, mcast_message* m)
{
  if (mcast_persist_tx_begin(p) != 0)
    return 0;

  if (mcast_persist_put_record(p, m) != 0) {
    mcast_log_error("Error persisting message uid = %" PRIu64, m->uid);
    mcast_persist_tx_abort(p);
    return 0;
  }

  if (mcast_persist_tx_commit(p) != 0)
    return 0;

  return 1;
}

static int
get_record(struct mcast_persist* p, m_uid_t uid, mcast_message* m)
{
  int rv = 0;
  if (mcast_persist_tx_begin(p) != 0)
    return 0;
  rv = mcast_persist_get_record(p, uid, m);
  if (mcast_persist_tx_commit(p) != 0)
    return 0;
  return rv;
}

static int
trim(struct mcast_persist* p, m_ts_t ts)
{
  int rv = 0;
  if (mcast_persist_tx_begin(p) != 0)
    return 0;
  rv = mcast_persist_trim(p, ts);
  if (mcast_persist_tx_commit(p) != 0)
    return 0;
  return rv;
}


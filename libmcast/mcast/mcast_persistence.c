/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast_persistence.h"
#include <stdlib.h>
#include <string.h>

void
mcast_persist_init(struct mcast_persist* store, int node_id, const char* suffix)
{
  if (!mcast_config.persist) {
    mcast_persist_init_hash(store, node_id);
    return;
  } else {
#ifdef HAS_LMDB
    mcast_persist_init_lmdb(store, node_id, suffix);
    return;
#endif
    mcast_log_error("Persistent storage not available!");
    exit(1);
  }
}

int
mcast_persist_open(struct mcast_persist* store)
{
  return store->api.open(store->handle);
}

void
mcast_persist_close(struct mcast_persist* store)
{
  store->api.close(store->handle);
}

int
mcast_persist_tx_begin(struct mcast_persist* store)
{
  return store->api.tx_begin(store->handle);
}

int
mcast_persist_tx_commit(struct mcast_persist* store)
{
  return store->api.tx_commit(store->handle);
}

void
mcast_persist_tx_abort(struct mcast_persist* store)
{
  store->api.tx_abort(store->handle);
}

int
mcast_persist_get_record(struct mcast_persist* store, m_uid_t uid, mcast_message* out)
{
  return store->api.get(store->handle, uid, out);
}

int
mcast_persist_put_record(struct mcast_persist* store, mcast_message* msg)
{
  return store->api.put(store->handle, msg);
}

int
mcast_persist_get_record_count(struct mcast_persist* store)
{
  return store->api.size(store->handle);
}

int
mcast_persist_trim(struct mcast_persist* store, m_ts_t ts)
{
  return store->api.trim(store->handle, ts);
}

m_ts_t
mcast_persist_get_trim(struct mcast_persist* store)
{
  return store->api.get_trim(store->handle);
}

char*
mcast_message_to_buffer(mcast_message* msg, int* size)
{
  size_t len = msg->value.mcast_value_len;
  *size = sizeof(mcast_message) + len;
  char* buffer = malloc(*size);
  if (buffer == NULL) {
    *size = 0;
    return NULL;
  }
  memcpy(buffer, msg, sizeof(mcast_message));
  if (len > 0) {
    memcpy(&buffer[sizeof(mcast_message)], msg->value.mcast_value_val, len);
  }
  return buffer;
}

void
mcast_message_from_buffer(char* buffer, mcast_message* out)
{
  memcpy(out, buffer, sizeof(mcast_message));
  if (out->value.mcast_value_len > 0) {
    out->value.mcast_value_val = malloc(out->value.mcast_value_len);
    memcpy(out->value.mcast_value_val, &buffer[sizeof(mcast_message)], out->value.mcast_value_len);
  } else {
    out->value.mcast_value_val = NULL;
  }
}

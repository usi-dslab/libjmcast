/*
 * Copyright (c) 2014, Xiaoguang Sun <sun dot xiaoguang at yoyosys dot com>
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast_persistence.h"
#include <assert.h>
#include <errno.h>
#include <inttypes.h>
#include <lmdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct lmdb_persist
{
  MDB_env*    env;
  MDB_txn*    txn;
  MDB_dbi     dbi;
  int         node_id;
  const char* suffix;
};

static void lmdb_persist_close(void* handle);

static int
lmdb_compare_uid(const MDB_val* lhs, const MDB_val* rhs)
{
  m_uid_t lid, rid;
  assert(lhs->mv_size == sizeof(m_uid_t));
  assert(rhs->mv_size == sizeof(m_uid_t));
  lid = *((m_uid_t*)lhs->mv_data);
  rid = *((m_uid_t*)rhs->mv_data);
  return (lid == rid) ? 0 : (lid < rid) ? -1 : 1;
}

static int
lmdb_persist_init(struct lmdb_persist* s, char* db_env_path)
{
  int      result;
  MDB_env* env = NULL;
  MDB_txn* txn = NULL;
  MDB_dbi  dbi = 0;

  if ((result = mdb_env_create(&env)) != 0) {
    mcast_log_error("Could not create lmdb environment. %s", mdb_strerror(result));
    goto error;
  }
  if ((result = mdb_env_set_mapsize(env, mcast_config.lmdb_mapsize)) != 0) {
    mcast_log_error("Could not set lmdb map size. %s", mdb_strerror(result));
    goto error;
  }
  if ((result = mdb_env_open(env, db_env_path, (!mcast_config.lmdb_sync ? (MDB_NOSYNC | MDB_NOMETASYNC) : 0),
                             S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH)) != 0) {
    mcast_log_error("Could not open lmdb environment at %s. %s", db_env_path, mdb_strerror(result));
    goto error;
  }
  if ((result = mdb_txn_begin(env, NULL, 0, &txn)) != 0) {
    mcast_log_error("Could not start txn on lmdb environment at %s. %s", db_env_path, mdb_strerror(result));
    goto error;
  }
  if ((result = mdb_open(txn, NULL, MDB_INTEGERKEY, &dbi)) != 0) {
    mcast_log_error("Could not open db on lmdb environment at %s. %s", db_env_path, mdb_strerror(result));
    goto error;
  }
  if ((result = mdb_set_compare(txn, dbi, lmdb_compare_uid)) != 0) {
    mcast_log_error("Could setup compare function on lmdb "
              "environment at %s. %s",
              db_env_path, mdb_strerror(result));
    goto error;
  }
  if ((result = mdb_txn_commit(txn)) != 0) {
    mcast_log_error("Could commit txn on lmdb environment at %s. %s", db_env_path, mdb_strerror(result));
    goto error;
  }

  s->env = env;
  s->dbi = dbi;

  return 0;
error:
  if (txn) {
    mdb_txn_abort(txn);
  }
  if (dbi) {
    mdb_close(env, dbi);
  }
  if (env) {
    mdb_env_close(env);
  }
  return -1;
}

static struct lmdb_persist*
lmdb_persist_new(int node_id, const char* suffix)
{
  struct lmdb_persist* s = malloc(sizeof(struct lmdb_persist));
  memset(s, 0, sizeof(struct lmdb_persist));
  s->node_id = node_id;
  s->suffix = suffix;
  return s;
}

static int
lmdb_persist_open(void* handle)
{
  struct lmdb_persist* s = handle;
  char*                lmdb_env_path = NULL;
  struct stat          sb;
  int                  dir_exists, result;
  size_t lmdb_env_path_length = strlen(mcast_config.lmdb_env_path) + 16 + (s->suffix == NULL ? 0 : strlen(s->suffix));

  lmdb_env_path = malloc(lmdb_env_path_length);
  snprintf(lmdb_env_path, lmdb_env_path_length, "%s_%6.6d_%s", mcast_config.lmdb_env_path, s->node_id,
           (s->suffix == NULL ? "" : s->suffix));

  // Trash files -- testing only
  if (mcast_config.trash_files) {
    char rm_command[600];
    sprintf(rm_command, "rm -r %s", lmdb_env_path);
    result = system(rm_command);
    if(result == -1)
      mcast_log_error("Unable to delete trash old LMDB env path");
  }

  dir_exists = (stat(lmdb_env_path, &sb) == 0);

  if (!dir_exists && (mkdir(lmdb_env_path, S_IRWXU) != 0)) {
    mcast_log_error("Failed to create env dir %s: %s", lmdb_env_path, strerror(errno));
    result = -1;
    if (s != NULL) {
      lmdb_persist_close(s);
    }
  } else {
    if ((result = lmdb_persist_init(s, lmdb_env_path) != 0))
      mcast_log_error("Failed to open DB handle");
    else
      mcast_log_info("lmdb storage opened successfully");
  }
  free(lmdb_env_path);
  return result;
}

static void
lmdb_persist_close(void* handle)
{
  struct lmdb_persist* s = handle;
  if (s->txn) {
    mdb_txn_abort(s->txn);
  }
  if (s->dbi) {
    mdb_close(s->env, s->dbi);
  }
  if (s->env) {
    mdb_env_close(s->env);
  }
  free(s);
  mcast_log_info("lmdb storage closed successfully");
}

static int
lmdb_persist_tx_begin(void* handle)
{
  struct lmdb_persist* s = handle;
  assert(s->txn == NULL);
  return mdb_txn_begin(s->env, NULL, 0, &s->txn);
}

static int
lmdb_persist_tx_commit(void* handle)
{
  struct lmdb_persist* s = handle;
  int                  result;
  assert(s->txn);
  result = mdb_txn_commit(s->txn);
  s->txn = NULL;
  return result;
}

static void
lmdb_persist_tx_abort(void* handle)
{
  struct lmdb_persist* s = handle;
  if (s->txn) {
    mdb_txn_abort(s->txn);
    s->txn = NULL;
  }
}

static int
lmdb_persist_get(void* handle, m_uid_t uid, mcast_message* out)
{
  struct lmdb_persist* s = handle;
  int                  result;
  MDB_val              key, data;

  memset(&data, 0, sizeof(data));
  key.mv_data = &uid;
  key.mv_size = sizeof(m_uid_t);

  if ((result = mdb_get(s->txn, s->dbi, &key, &data)) != 0) {
    if (result == MDB_NOTFOUND) {
      mcast_log_debug("There is no record for uid: %" PRIu64, uid);
    } else {
      mcast_log_error("Error retrieving record for uid %" PRIu64 " : %s", uid, mdb_strerror(result));
    }
    return 0;
  }

  if (out)
    mcast_message_from_buffer(data.mv_data, out);

  return 1;
}

static int
lmdb_persist_put(void* handle, mcast_message* msg)
{
  struct lmdb_persist* s = handle;
  int                  result, buffer_size;
  MDB_val              key, data;
  char*                buffer = mcast_message_to_buffer(msg, &buffer_size);

  key.mv_data = &msg->uid;
  key.mv_size = sizeof(m_uid_t);

  data.mv_data = buffer;
  data.mv_size = buffer_size;

  result = mdb_put(s->txn, s->dbi, &key, &data, 0);
  free(buffer);
  return result;
}

static int
lmdb_persist_count(void* handle)
{
  struct lmdb_persist* s = handle;
  MDB_stat             stat;
  mdb_env_stat(s->env, &stat);
  return stat.ms_entries;
}

static m_ts_t
lmdb_persist_get_trim(void* handle)
{
  struct lmdb_persist* s = handle;
  int                  result;
  m_uid_t              uid = 0, k = 0;
  MDB_val              key, data;

  key.mv_data = &k;
  key.mv_size = sizeof(m_uid_t);

  if ((result = mdb_get(s->txn, s->dbi, &key, &data)) != 0) {
    if (result != MDB_NOTFOUND) {
      mcast_log_error("mdb_get failed: %s", mdb_strerror(result));
      assert(result == 0);
    } else {
      uid = 0;
    }
  } else {
    uid = *(m_uid_t*)data.mv_data;
  }

  return uid;
}

static int
lmdb_persist_put_trim(void* handle, m_ts_t ts)
{
  struct lmdb_persist* s = handle;
  m_uid_t              k = 0, uid2ts = ts;
  int                  result;
  MDB_val              key, data;

  key.mv_data = &k;
  key.mv_size = sizeof(m_uid_t);

  data.mv_data = &uid2ts;
  data.mv_size = sizeof(m_uid_t);

  result = mdb_put(s->txn, s->dbi, &key, &data, 0);
  if (result != 0)
    mcast_log_error("%s", mdb_strerror(result));

  return result;
}

static int
lmdb_persist_trim(void* handle, m_ts_t ts)
{
  struct lmdb_persist* s = handle;
  int                  result;
  m_uid_t              min = 0;
  m_ts_t               last_trim = 0;
  MDB_cursor*          cursor = NULL;
  MDB_val              key, data;

  last_trim = lmdb_persist_get_trim(handle);

  if (ts == 0 || last_trim > ts || lmdb_persist_put_trim(handle, ts) != 0)
    return 1;

  if ((result = mdb_cursor_open(s->txn, s->dbi, &cursor)) != 0) {
    mcast_log_error("Could not create cursor. %s", mdb_strerror(result));
    goto cleanup_exit;
  }

  key.mv_data = &min;
  key.mv_size = sizeof(m_uid_t);

  do {
    if ((result = mdb_cursor_get(cursor, &key, &data, MDB_NEXT)) == 0) {
      assert(key.mv_size == sizeof(m_uid_t));
      min = *(m_uid_t*)key.mv_data;
      last_trim = ((mcast_message*)data.mv_data)->timestamp;
    } else {
      goto cleanup_exit;
    }

    if (min != 0 && last_trim <= ts) {
      if (mdb_cursor_del(cursor, 0) != 0) {
        mcast_log_error("mdb_cursor_del failed. %s", mdb_strerror(result));
        goto cleanup_exit;
      }
    }
  } while (1); // (min <= uid);

cleanup_exit:
  if (cursor) {
    mdb_cursor_close(cursor);
  }
  return 0;
}

void
mcast_persist_init_lmdb(struct mcast_persist* s, int node_id, const char* suffix)
{
  s->handle = lmdb_persist_new(node_id, suffix);
  s->api.open = lmdb_persist_open;
  s->api.close = lmdb_persist_close;
  s->api.tx_begin = lmdb_persist_tx_begin;
  s->api.tx_commit = lmdb_persist_tx_commit;
  s->api.tx_abort = lmdb_persist_tx_abort;
  s->api.get = lmdb_persist_get;
  s->api.put = lmdb_persist_put;
  s->api.size = lmdb_persist_count;
  s->api.trim = lmdb_persist_trim;
  s->api.get_trim = lmdb_persist_get_trim;
}

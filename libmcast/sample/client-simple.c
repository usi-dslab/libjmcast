/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define VSIZE 20000

struct client_value
{
  struct timeval t;
  short          global;
  size_t         size;
  unsigned long  key;
  char           value[VSIZE];
};

struct client
{
  struct client_value  v;
  struct mcast_client* c;
};

// client configuration parameters - default values
static int         node_id = 0;
static int         group_id = 0;
static int         global = 0;
static int         verbose = 0;
static int         msg_size = 1024;
static const char* config = "";

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d\n", sig);
  event_base_loopexit(base, NULL);
}

static void
random_string(char* s, const int len)
{
  int               i;
  static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  for (i = 0; i < len - 1; ++i)
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  s[len - 1] = 0;
}

static void
client_submit_value(struct client* c)
{
  m_gid_t dst[MAX_N_OF_GROUPS];
  int     dst_len;

  gettimeofday(&c->v.t, NULL);
  c->v.key = rand();

  if (rand() % 100 < global) {
    c->v.global = 1;
    dst_len = 3;
    dst[0] = group_id;
    dst[1] = (group_id + 1) % mcast_client_get_group_count(c->c);
    dst[2] = (group_id + 2) % mcast_client_get_group_count(c->c);
  } else {
    c->v.global = 0;
    dst_len = 1;
    dst[0] = group_id;
  }

  if (verbose)
    printf("Client sending message '%.16s' to %s\n", c->v.value, (c->v.global ? "some/all groups" : "own group"));

  mcast_client_submit(c->c, (char*)&c->v, sizeof(struct client_value) - VSIZE + c->v.size, dst, dst_len);
}

static void
on_connect(struct mcast_client* mc, void* arg)
{
  struct client* c = arg;

  random_string(c->v.value, msg_size);
  c->v.size = msg_size;
  client_submit_value(c);
}

static void
on_read(struct mcast_client* mc, char* resp, int resp_len, void* ctx)
{
  struct client*       c = ctx;
  struct client_value* v = (struct client_value*)resp;
  if (resp_len) {
    if (verbose)
      printf("Client %" PRIu64 " received %zu bytes - key %lu, value %.16s\n", mcast_client_get_uid(mc), v->size,
             v->key, v->value);

    if (strcmp(v->value, c->v.value))
      printf("Client %" PRIu64 " received a value different from the one it submitted\n", mcast_client_get_uid(mc));

    client_submit_value(c);
  }
}

static struct client*
make_client(struct event_base* base)
{
  struct client* c;

  c = malloc(sizeof(struct client));
  c->c = mcast_client_connect(group_id, node_id, config, base, on_connect, on_read, c);
  if (c->c == NULL) {
    printf("Error connecting, exiting...\n");
    free(c);
    return NULL;
  }

  return c;
}

static void
client_free(struct client* c)
{
  mcast_client_free(c->c);
  free(c);
}

static void
usage(const char* prog)
{
  printf("Usage: %s -n <node-id> -g <group_id> -c <path/to/mcast.conf> [-a] <percentage> [-s] <msg-size] [-h] [-v]\n",
         prog);
  printf("  %-30s%s\n", "-n, --node-id", "the node id");
  printf("  %-30s%s\n", "-g, --group-id", "the group id");
  printf("  %-30s%s\n", "-c, --config-file", "path to multicast configuration file");
  printf("  %-30s%s\n", "-a, --multigroup-percentage", "porcentage of multigroup commands (defaults to 0)");
  printf("  %-30s%s%d%s\n", "-s, --message-size", "size of the message in bytes (defaults to ", msg_size, " bytes)");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose", "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                  opt = 0, idx = 0;
  struct client*       client;
  struct event_base*   base;
  struct event*        sig;
  static struct option options[] = { { "node-id", required_argument, 0, 'n' },
                                     { "group-id", required_argument, 0, 'g' },
                                     { "config-file", required_argument, 0, 'c' },
                                     { "multigroup-percentage", required_argument, 0, 'a' },
                                     { "message-size", required_argument, 0, 's' },
                                     { "verbose", no_argument, 0, 'v' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvn:g:c:a:s:", options, &idx)) != -1) {
    switch (opt) {
      case 'n':
        node_id = atoi(optarg);
        break;
      case 'g':
        group_id = atoi(optarg);
        break;
      case 'a':
        global = atoi(optarg);
        break;
      case 's':
        msg_size = atoi(optarg);
        break;
      case 'c':
        config = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  if (group_id == -1 || node_id == -1 || strlen(config) == 0)
    usage(argv[0]);

  srand(time(NULL));
  base = event_base_new();
  client = make_client(base);
  if (client) {
    sig = evsignal_new(base, SIGINT, handle_sigint, base);
    evsignal_add(sig, NULL);
    event_base_dispatch(base);
    client_free(client);
    event_free(sig);
  }

  event_base_free(base);
  return 0;
}

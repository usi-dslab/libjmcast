/*
 * Copyright (c) 2014-2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <evpaxos.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

static int                id = 0;
static struct event_base* acc_base;
const char*               config = "paxos.conf";

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d\n", sig);
  if (sig == SIGINT) {
    event_base_loopexit(base, NULL);
    event_base_loopexit(acc_base, NULL);
  }
}

static void*
start_proposer(void* arg)
{
  struct event*      sig;
  struct event_base* base;
  struct evproposer* prop;

  base = event_base_new();
  sig = evsignal_new(base, SIGINT, handle_sigint, base);
  evsignal_add(sig, NULL);
  prop = evproposer_init(id, config, base);
  if (prop == NULL) {
    printf("Could not start the proposer!\n");
    exit(1);
  }

  event_base_dispatch(base);
  event_free(sig);
  evproposer_free(prop);
  event_base_free(base);
  pthread_exit(NULL);
}

static void*
start_acceptor(void* arg)
{
  struct evacceptor* acc;
  struct event_base* base;

  base = event_base_new();
  acc = evacceptor_init(id, config, base);
  if (acc == NULL) {
    printf("Could not start the acceptor\n");
    exit(1);
  }

  acc_base = base;
  event_base_dispatch(base);
  evacceptor_free(acc);
  event_base_free(base);
  pthread_exit(NULL);
}

static void
start_replica()
{
  pthread_t pt, at;
  pthread_create(&at, NULL, start_acceptor, NULL);
  pthread_create(&pt, NULL, start_proposer, NULL);
  pthread_join(pt, NULL);
  pthread_join(at, NULL);
}

static void
usage(const char* prog)
{
  printf("Usage: %s id [path/to/paxos.conf] [-h] [-s]\n", prog);
  printf("  %-30s%s\n", "-h, --help", "Output this message and exit");
  exit(1);
}

int
main(int argc, char const* argv[])
{
  int i = 2;

  if (argc < 2)
    usage(argv[0]);

  id = atoi(argv[1]);
  if (argc >= 3 && argv[2][0] != '-') {
    config = argv[2];
    i++;
  }

  if (i != argc)
    usage(argv[0]);

  signal(SIGPIPE, SIG_IGN);
  start_replica();
  return 0;
}

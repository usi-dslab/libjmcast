package ch.usi.dslab.lel.jmcast.jni;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.ClientCallbackHandler;
import ch.usi.dslab.lel.jmcast.Util;

/**
 * Created by longle on 19.10.16.
 */
public class McastClientJNI {


//    static {
//        loadNativeLib();
//    }

    ClientCallbackHandler cbOnConnected;
    ClientCallbackHandler cbOnReply;

    public McastClientJNI(int clientId, String mconfig) {
        loadNativeLib();
        this.jni_init(clientId, mconfig);
    }

    public McastClientJNI(int clientId, String mconfig, ClientCallbackHandler cbOnConnected, ClientCallbackHandler cbOnReply) {
        loadNativeLib();
        this.jni_init(clientId, mconfig);
        this.cbOnConnected = cbOnConnected;
        this.cbOnReply = cbOnReply;
        this.jni_start();
    }

    private void loadNativeLib() {
        try {
            Util.loadLibrary("McastClient");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public native void jni_init(int clientId, String mconfig);

    public native void jni_start();

    public native void jni_multicast(int[] groupId, int cmdId, byte[] message);

    public native void jni_connect_to_node(int groupId, int nodeId);

    public void onConnected(String data, int groupId, int nodeId) {

        if (data.equals(ClientCallbackHandler.CONNECT_OK)) {
            cbOnConnected.success(new Message(ClientCallbackHandler.CONNECT_OK, groupId, nodeId));
        } else {
            cbOnConnected.error(new Message(ClientCallbackHandler.CONNECT_ERROR, groupId, nodeId));
        }
    }

    public void onReply(byte[] message) {
        try {
            Message reply = Message.createFromBytes(message);
            cbOnReply.success(reply);
        } catch (com.esotericsoftware.kryo.KryoException e) {
            Message reply = new Message("CORRUPTED");
            reply.setCorrupted(true);
            cbOnReply.success(reply);
        } catch (java.lang.ClassCastException e) {
            Message reply = new Message("CORRUPTED");
            reply.setCorrupted(true);
            cbOnReply.success(reply);
        }
    }

    public void setCallback(ClientCallbackHandler cbOnConnected, ClientCallbackHandler cbOnReply) {
        this.cbOnConnected = cbOnConnected;
        this.cbOnReply = cbOnReply;
    }

    public void setReplyCallback(ClientCallbackHandler cbOnReply) {
        this.cbOnReply = cbOnReply;
    }

//    private void multicast(int[] groupId, String message) {
//        this.multicast(groupId, 0, message);
//    }

    public void setConnectedCallback(ClientCallbackHandler cbOnConnected) {
        this.cbOnConnected = cbOnConnected;
    }

//    private void multicast(int[] groupId, int cmdId, String message) {
//        this.jni_multicast(groupId, cmdId, Message.getBytes(message));
//    }
//
//    private void multicast(int[] groupId, int cmdId, byte[] message) {
//        this.jni_multicast(groupId, cmdId, message);
//    }

    public void start() {
        this.jni_start();
    }

//    public void multicast(int[] groupId, int cmdId, Message message) {
//        this.multicast(groupId, cmdId, message.serializeToString());
//    }

    private void multicast(int[] groupId, byte[] message) {
        this.jni_multicast(groupId, 0, message);
    }

    public void multicast(int[] groupId, Message message) {

//        this.multicast(groupId, bytes);
        this.multicast(groupId, Message.getBytes(message));
    }
}

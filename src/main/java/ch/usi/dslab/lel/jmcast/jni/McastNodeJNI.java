package ch.usi.dslab.lel.jmcast.jni;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.NodeCallbackHandler;
import ch.usi.dslab.lel.jmcast.Util;

/**
 * Created by longle on 19.10.16.
 */
public class McastNodeJNI extends Thread {

    public native void jni_init(int groupId, int nodeId, String mconfig, String pconfig);

    public native void jni_reply_to_client(int clientId, byte[] message);

    public native int jni_is_connected_to_client(int clientId);

    public native void jni_start();

//    Message msg = new Message(1);

    //    byte[] reply = new byte[1000];
    NodeCallbackHandler cbOnDeliver;

    static {
        try {
            Util.loadLibrary("McastNode");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public boolean isConnectedToClient(int clientId) {
        if (this.jni_is_connected_to_client(clientId) == 0) return false;
        return true;
    }

    public void deliver(int clientId, byte[] message) {
//        cbOnDeliver.onDeliver(clientId, msg);
        try {
            Message reply = Message.createFromBytes(message);
            cbOnDeliver.onDeliver(clientId, reply);
        } catch (com.esotericsoftware.kryo.KryoException e) {
            Message reply = new Message("CORRUPTED");
            reply.setCorrupted(true);
            cbOnDeliver.onDeliver(clientId, reply);
        } catch (java.lang.ClassCastException e) {
            Message reply = new Message("CORRUPTED");
            reply.setCorrupted(true);
            cbOnDeliver.onDeliver(clientId, reply);
        }

    }

    private void reply(int clientId, byte[] message) {
        if (this.isConnectedToClient(clientId)) this.jni_reply_to_client(clientId, message);
    }

    public void reply(int clientId, Message message) {
//        this.reply(clientId, reply);
        this.reply(clientId, Message.getBytes(message));
    }

    public void setCallback(NodeCallbackHandler cbOnDeliver) {
        this.cbOnDeliver = cbOnDeliver;
    }

    public McastNodeJNI(int groupId, int nodeId, String mconfig, String pconfig) {
        this.jni_init(groupId, nodeId, mconfig, pconfig);
    }

    public void start() {
        this.jni_start();
    }

    @Override
    public void run() {
        this.start();
    }

}

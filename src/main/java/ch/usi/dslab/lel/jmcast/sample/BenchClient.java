package ch.usi.dslab.lel.jmcast.sample;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.ClientCallbackHandler;
import ch.usi.dslab.lel.jmcast.jni.McastClientJNI;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by longle on 20.10.16.
 */
public class BenchClient {
    private static final int GROUP_COUNT = 3;
    McastClientJNI mcastClient;
    int clientId;
    String config;
    static final int messageSize = 1000;
    static final Random random = new Random(); // Or SecureRandom
    static int[] dest = new int[]{0};
    static int[] dest_global = new int[]{0, 1};
    static int destinationCount = 0;

    //    ThroughputPassiveMonitor tpMonitor = null;
//    LatencyPassiveMonitor latencyMonitor;
    long sentTime;
    AtomicInteger count = new AtomicInteger(0);
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static SecureRandom rnd = new SecureRandom();
    String randomString = randomString(messageSize);

    String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public BenchClient(int clientId, String config, int destinationCount) {
        this.clientId = clientId;
        this.config = config;
        this.destinationCount = destinationCount;
//        latencyMonitor = new LatencyPassiveMonitor(clientId, "jmcast_client");
//        tpMonitor = new ThroughputPassiveMonitor(clientId, "jmcast_client");
        System.out.println("Starting client " + clientId);
        mcastClient = new McastClientJNI(clientId, config);

        int groupsCount = 2;
        final int[] connectedGroups = {0};
        final boolean[] isReady = {false};
        ClientCallbackHandler cbOnConnected = reply -> {
            int groupId = (int) reply.getItem(1);
            int nodeId = (int) reply.getItem(2);


            connectedGroups[0] += 1;
            if (groupsCount == connectedGroups[0]) {
                System.out.println("JMcastClient " + clientId + " is ready");
                isReady[0] = true;
            } else {
                System.out.println("JMcastClient waiting...");
            }

        };
        Msg rndMsg = new Msg(randomString);
//        ClientCallbackHandler cbOnConnected = reply -> {
//            System.out.println("client " + clientId + " connected");
//            this.sendMessage(new Msg(randomString(random.nextInt(messageSize))));
//        };
        ClientCallbackHandler cbOnReply = reply -> {
//            latencyMonitor.logLatency(sentTime, System.nanoTime());
//            System.out.println("client " + clientId + " recevied message " + reply);
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            this.sendMessage(rndMsg);
//            this.test(new Msg(randomString(messageSize)));
        };

        mcastClient.setCallback(cbOnConnected, cbOnReply);
        mcastClient.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("JMcastClient start sending message...");
        sendMessage(rndMsg);
//        mcastClient.jni_connect_to_node(0, 0);
//        mcastClient.jni_connect_to_node(1, 0);
    }


    public static void main(String[] args) throws InterruptedException {
        int clientId = args.length == 0 ? 1 : Integer.parseInt(args[0]);
        String mcastConfig = args[1];
        int destinationCount = Integer.parseInt(args[2]);

//        String gathererHost = args[3];
//        int gathererPort = Integer.parseInt(args[4]);
//        int duration = Integer.parseInt(args[5]);
//        String gathererLocation = args[6];
//        DataGatherer.setDuration(duration);
//        DataGatherer.enableFileSave(gathererLocation);
//        DataGatherer.enableGathererSend(gathererHost, gathererPort);

        BenchClient client = new BenchClient(clientId, mcastConfig, destinationCount);
        //keep thread running
        final Object forever = new Object();
        synchronized (forever) {
            try {
                forever.wait();
            } catch (InterruptedException ignore) {
            }
        }

    }

    private void sendMessage(Msg msg) {
//        tpMonitor.incrementCount();
//        sentTime = System.nanoTime();
//        Message m = new Message(msg);
//        int[] g = new int[1];
//        g[0] = 0;
//        mcastClient.multicast(g, new Message(msg));
//        return;


//        System.out.println(m.serializeToString().length());
//        System.out.println(m.serializeToString());
//        System.out.println("sending #" + count.incrementAndGet() + ":" + msg.toString());
//        int groups = random.nextInt(GROUP_COUNT) + 1;
        int groups = destinationCount;
        int[] destinations = new int[groups];
        for (int i = 0; i < groups; i++) {
            destinations[i] = i;
        }
//        System.out.print("send another one to ");
//        for (int i = 0; i < groups; i++) {
//            System.out.print(destinations[i] + " ");
//        }
//        System.out.println();
//        if (chance <= destinationCount)
        mcastClient.multicast(destinations, new Message(msg));
//        else
//            mcastClient.multicast(dest, new Message(msg));
    }
}

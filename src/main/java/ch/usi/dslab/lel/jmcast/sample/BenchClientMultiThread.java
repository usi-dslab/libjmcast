package ch.usi.dslab.lel.jmcast.sample;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.LatencyPassiveMonitor;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.jmcast.ClientCallbackHandler;
import ch.usi.dslab.lel.jmcast.jni.McastClientJNI;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by longle on 20.10.16.
 */
public class BenchClientMultiThread {
    static final int messageSize = 1000;
    static final Random random = new Random(); // Or SecureRandom
    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final int GROUP_COUNT = 2;
    static int[] dest = new int[]{0};
    static int[] dest_global = new int[]{0, 1};
    static float globalRate = 0;
    static SecureRandom rnd = new SecureRandom();
    static Semaphore permit;
    static boolean permitTest = false;
    private static ExecutorService executorService;
    McastClientJNI mcastClient;
    int clientId;
    String config;
    ThroughputPassiveMonitor tpMonitor = null;
    LatencyPassiveMonitor latencyMonitor;
    long sentTime;
    AtomicInteger count = new AtomicInteger(0);

    public BenchClientMultiThread(int clientId, String config) {
        this.clientId = clientId;
        this.config = config;
        latencyMonitor = new LatencyPassiveMonitor(clientId, "jmcast_client");
        tpMonitor = new ThroughputPassiveMonitor(clientId, "jmcast_client");
        System.out.println("Starting client " + clientId);
        mcastClient = new McastClientJNI(clientId, config);

        int groupsCount = 2;
        final int[] connectedGroups = {0};
        final boolean[] isReady = {false};

        ClientCallbackHandler cbOnConnected = reply -> {
            int groupId = (int) reply.getItem(1);
            int nodeId = (int) reply.getItem(2);


            connectedGroups[0] += 1;
            if (groupsCount == connectedGroups[0]) {
                System.out.println("JMcastClient " + clientId + " is ready");
                isReady[0] = true;
            } else {
                System.out.println("JMcastClient waiting...");
            }

        };

        ClientCallbackHandler cbOnReply = reply -> {
            latencyMonitor.logLatency(sentTime, System.nanoTime());
//            System.out.println(this.clientId + "::recevied::" + reply.toString().substring(0, 20));
            if (permitTest)
                permit.release();
        };

        mcastClient.setCallback(cbOnConnected, cbOnReply);
        mcastClient.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int clientId = args.length == 0 ? 1 : Integer.parseInt(args[0]);
        String mcastConfig = args[1];
        float globalRate = Float.parseFloat(args[2]);

        String gathererHost = args[3];
        int gathererPort = Integer.parseInt(args[4]);
        int duration = Integer.parseInt(args[5]);
        String gathererLocation = args[6];
        DataGatherer.setDuration(duration);
        DataGatherer.enableFileSave(gathererLocation);
        DataGatherer.enableGathererSend(gathererHost, gathererPort);

        int numPermit = 2;
        permit = new Semaphore(numPermit);
        permitTest = true;

        BenchClientMultiThread client = new BenchClientMultiThread(clientId, mcastConfig);
        client.globalRate = globalRate;
        client.runSingleThreadPermitTest();

//        executorService = Executors.newFixedThreadPool(numPermit);
//        permitTest = false;
//        for (int i = 0; i < numPermit; i++) {
//            int finalI = i;
//            executorService.submit(() -> {
//                BenchClientMultiThread client = new BenchClientMultiThread(clientId + finalI, mcastConfig);
//                client.globalRate = globalRate;
//                client.runSingleThreadPermitTest();
//            });
//        }

        //keep thread running
        final Object forever = new Object();
        synchronized (forever) {
            try {
                forever.wait();
            } catch (InterruptedException ignore) {
            }
        }

    }

    private static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    void getPermit() {
        try {
            permit.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void runSingleThreadPermitTest() {
        while (true) {
            sendMessage();
        }
    }

    private void sendMessage() {
        if (permitTest) getPermit();
        float chance = random.nextFloat();
        tpMonitor.incrementCount();
        sentTime = System.nanoTime();
        Message msg = new Message(count.incrementAndGet() + "@" + randomString(random.nextInt(messageSize) + 20));
//        System.out.println(this.clientId + "::sending ::" + msg.toString().substring(0, 20));
//        int groups = 1;
        int groups = random.nextInt(GROUP_COUNT) + 1;
        int[] destinations = new int[groups];
        for (int i = 0; i < groups; i++) {
            destinations[i] = i;
        }

//        System.out.print("send another one to ");
//        for (int i = 0; i < groups; i++) {
//            System.out.print(destinations[i] + " ");
//        }
//        System.out.println();

        mcastClient.multicast(destinations, msg);
    }
}

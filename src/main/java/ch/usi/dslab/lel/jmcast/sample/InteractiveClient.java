package ch.usi.dslab.lel.jmcast.sample;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.lel.jmcast.ClientCallbackHandler;
import ch.usi.dslab.lel.jmcast.jni.McastClientJNI;

import java.util.Scanner;

/**
 * Created by longle on 20.10.16.
 */
public class InteractiveClient {
    McastClientJNI mcastClient;
    int clientId;
    String config;
    static int[] dest1 = new int[]{0};
    static int[] dest2 = new int[]{1};
    static int[] destAll = new int[]{0, 1};

    public InteractiveClient(int clientId, String config) {
        this.clientId = clientId;
        this.config = config;
        System.out.println("Starting client " + clientId);
        mcastClient = new McastClientJNI(clientId, config);
        ClientCallbackHandler cbOnConnected = reply -> {
            System.out.println("InteractiveClient on connected" + reply.toString());
        };
        ClientCallbackHandler cbOnReply = reply -> {
            System.out.println("InteractiveClient on reply " + reply.toString());
        };
        mcastClient.setCallback(cbOnConnected, cbOnReply);
        mcastClient.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        mcastClient.jni_connect_to_node(0, 0);
    }

    public void runInteractive() {
        Scanner scan = new Scanner(System.in);
        String input;

        System.out.print("message to send: ");
        input = scan.nextLine();
        while (!input.equalsIgnoreCase("end")) {
            int ensemble = Integer.parseInt(input.split(" ")[0]);
            String message = input.split(" ", 2)[1];
            int[] dest;
            if (ensemble == 0) dest = dest1;
            else if (ensemble == 1) dest = dest2;
            else dest = destAll;
            mcastClient.multicast(dest, new Message(new Msg(input)));
            System.out.print("message to send: ");
            input = scan.nextLine();
        }
        scan.close();
    }

    public static void main(String[] args) throws InterruptedException {
        int clientId = args.length == 0 ? 1 : Integer.parseInt(args[0]);
        String mcastConfig = args[1];
        InteractiveClient client = new InteractiveClient(clientId, mcastConfig);

        client.runInteractive();
    }

}

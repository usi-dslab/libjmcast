package ch.usi.dslab.lel.jmcast.sample;

/**
 * Created by longle on 18.01.17.
 */
public class Msg {
    String content;

    public Msg() {
    }

    public Msg(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return content.toString();
    }
}

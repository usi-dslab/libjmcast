package ch.usi.dslab.lel.jmcast.sample;

import ch.usi.dslab.bezerra.netwrapper.Message;
import ch.usi.dslab.bezerra.sense.DataGatherer;
import ch.usi.dslab.bezerra.sense.monitors.ThroughputPassiveMonitor;
import ch.usi.dslab.lel.jmcast.NodeCallbackHandler;
import ch.usi.dslab.lel.jmcast.jni.McastNodeJNI;

/**
 * Created by longle on 20.10.16.
 */
public class Server {
    static ThroughputPassiveMonitor tpMonitor = null;

    public static void main(String[] args) throws InterruptedException {
        int groupId = Integer.parseInt(args[0]);
        int nodeId = Integer.parseInt(args[1]);
        String mcastConfig = args[2];
        String paxosConfig = args[3];

        String gathererHost = args[4];
        int gathererPort = Integer.parseInt(args[5]);
        int duration = Integer.parseInt(args[6]);
        String gathererLocation = args[7];
        DataGatherer.setDuration(duration);
        DataGatherer.enableFileSave(gathererLocation);
        DataGatherer.enableGathererSend(gathererHost, gathererPort);
        tpMonitor = new ThroughputPassiveMonitor(groupId * 100 + nodeId, "jmcast_learner", false);
        McastNodeJNI mcastNode = new McastNodeJNI(groupId, nodeId, mcastConfig, paxosConfig);
//        AtomicInteger count = new AtomicInteger(0);
        Message reply = new Message(1);
        NodeCallbackHandler cbOnDeliver = (clientId, msg) -> {
//            System.out.println("[" + groupId + "/" + nodeId + "]receive " + msg.toString().substring(0, 16) + " from " + clientId);
//            count.incrementAndGet();

            tpMonitor.incrementCount();
            if (groupId == 0 && nodeId == 0) mcastNode.reply(clientId, msg);

        };
        mcastNode.setCallback(cbOnDeliver);
        mcastNode.start();
    }
}

/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <jni.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdarg.h>
#include <pthread.h>
#include <event2/thread.h>

#define VSIZE 32768
#define MAX_CLIENT_THREADS 1000

struct client_value
{
  struct timeval t;
  short          global;
  size_t         size;
  unsigned long  key;
  int            client_id;
  char           value[VSIZE];

};

struct client
{
  struct client_value  v;
  struct mcast_client **mcast_clients;
  int mcast_clients_count;
  int client_id;
  struct event_base *base;
  pthread_mutex_t lock[MAX_CLIENT_THREADS];
};

struct connection {
    struct client *client;
    int group_id;
    int node_id;
};


// client configuration parameters - default values
static int         node_id = 0;
static int         group_id = 0;
static int         group_count = 0;
static int         verbose = 0;
static const char* config = "";


static JavaVM *g_jvm;
static jobject g_obj;
static jmethodID g_cb_on_reply, g_cb_on_connected;

static struct client *client;
static int client_id;

jint
JNI_OnLoad(JavaVM *vm, void *reserved) {
    g_jvm = vm;
    JNIEnv *env;
    if ((*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        printf("NATIVE jni failed");
        return -1;
    }
    return JNI_VERSION_1_8;
}

void trigger_callback(jmethodID callback, char *s, int len) {
    JNIEnv *env;

    int getEnvStat = (*g_jvm)->GetEnv(g_jvm, (void **) &env, JNI_VERSION_1_8);
    if (getEnvStat == JNI_EDETACHED) {
        int ret = (*g_jvm)->AttachCurrentThread(g_jvm, (void **) &env, NULL);
        if (ret!=0) {
            printf("Failed to attach. Error: %d\n", ret);
        }
    } else if (getEnvStat == JNI_EVERSION) {
        printf("GetEnv: version not supported\n");
    }

    jbyteArray js = (*env)->NewByteArray(env, len);

    (*env)->SetByteArrayRegion(env, js, 0, len, s);
    (*env)->CallVoidMethod(env, g_obj, callback, js);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }
    (*env)->DeleteLocalRef(env, js);
    (*g_jvm)->DetachCurrentThread(g_jvm);
}

void trigger_callback_on_connect(jmethodID callback, char *s, int group_id, int node_id) {
    JNIEnv *env;

    // double check it's all ok
    int getEnvStat = (*g_jvm)->GetEnv(g_jvm, (void **) &env, JNI_VERSION_1_8);
    if (getEnvStat == JNI_EDETACHED) {
        int ret = (*g_jvm)->AttachCurrentThread(g_jvm, (void **) &env, NULL);
        if (ret!=0) {
            printf("Failed to attach. Error: %d\n", ret);
        }
    } else if (getEnvStat == JNI_EVERSION) {
        printf("GetEnv: version not supported\n");
    } else if (getEnvStat == JNI_OK) {
        printf("GetEnv: OK\n");
    }
    jstring js = (*env)->NewStringUTF(env, s);
    /* Initializing arguments to store all values after s */
    (*env)->CallVoidMethod(env, g_obj, callback, js, group_id, node_id);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }
    (*env)->DeleteLocalRef(env, js);
    (*g_jvm)->DetachCurrentThread(g_jvm);
}


JNIEXPORT void JNICALL Java_ch_usi_dslab_lel_jmcast_jni_McastClientJNI_jni_1multicast___3II_3B
  (JNIEnv *env, jobject obj, jintArray group_ids, jint cmd_id, jbyteArray message){

    jbyte* bufferPtr = (*env)->GetByteArrayElements(env, message, 0);
    jsize lengthOfArray = (*env)->GetArrayLength(env, message);

    memcpy(client->v.value,bufferPtr,lengthOfArray);
    client->v.size = lengthOfArray;
    client->v.client_id = client->client_id;

    jsize j_dst_len = (*env)->GetArrayLength(env, group_ids);
    jint *j_group_ids = (*env)->GetIntArrayElements(env, group_ids, 0);

    m_gid_t dst[j_dst_len];
    int dst_len = (int) j_dst_len;
    for (int i = 0; i < j_dst_len; i++)
    dst[i] = j_group_ids[i];
    int group_id = dst[client->client_id % dst_len];


    gettimeofday(&client->v.t, NULL);

    if (dst_len > 1) {
        client->v.global = 1;
    } else {
        client->v.global = 0;
    }

    size_t size = sizeof(struct client_value) - VSIZE + client->v.size;
    if (verbose) {
        printf("[Client %d]: sending message size %zu to group ", client->v.client_id,client->v.size);
        for (int i=0; i < dst_len; i++) {
            printf("%d ", dst[i]);
        }
        printf("\n");
    }

    pthread_mutex_lock(&client->lock[group_id]);
    mcast_client_submit(client->mcast_clients[group_id], (char *) &client->v, size, dst, dst_len);
    pthread_mutex_unlock(&client->lock[group_id]);

    (*env)->ReleaseByteArrayElements(env, message, bufferPtr, 0);
    (*env)->ReleaseIntArrayElements(env, group_ids, j_group_ids, 0);

}

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d\n", sig);
  event_base_loopexit(base, NULL);
}


static void
on_read(struct mcast_client* mc, char* resp, int resp_len, void* ctx)
{
  if (resp_len) {
    if (verbose)
      printf("[Client %d] received (%d) bytes, value %s\n", client->v.client_id, resp_len, resp);
      trigger_callback(g_cb_on_reply, resp, resp_len);
  }
}


static void on_connect_to_host(struct mcast_client* mc, void* arg) {
    struct connection *conn = arg;
    int gid  = conn->group_id;
    int nid  = conn->node_id;
    if (mc == NULL) {
        trigger_callback_on_connect(g_cb_on_connected, "CONNECT_ERROR", gid, nid);
    } else{
        trigger_callback_on_connect(g_cb_on_connected, "CONNECT_OK", gid, nid);
    }
}


JNIEXPORT void JNICALL Java_ch_usi_dslab_lel_jmcast_jni_McastClientJNI_jni_1connect_1to_1node
(JNIEnv *env, jobject obj, jint gid, jint nid) {
    if (verbose) printf("[Client %d] group count %d group %d node %d \n", client->client_id, group_count, gid, nid);
    if (client->mcast_clients_count==0) {
        client->mcast_clients = malloc(sizeof(struct mcast_client*) * group_count);
        client->mcast_clients_count = group_count;
    }
    if (verbose) printf("[Client %d] setting up to connect to g/n: %d/%d\n", client->client_id, gid, nid);
    struct connection* conn;
    conn = malloc(sizeof(struct connection*));
    conn->client = client;
    conn->group_id = gid;
    conn->node_id = nid;
    client->mcast_clients[gid] = mcast_client_connect(gid, nid, config, client->base, on_connect_to_host, on_read, conn);
    if (verbose) printf("[Client %d] finish setting up, trying to connect to g/n: %d/%d\n",client->client_id, gid, nid);
}

// Little cheat here: each client only connect to one node per group.
static void
connect_to_all_group(){
    if (client->mcast_clients_count==0) {
        client->mcast_clients = malloc(sizeof(struct mcast_client*) * group_count);
        client->mcast_clients_count = group_count;
    }
    int nid = 0;
    int ok = 1;
    for (int gid = 0; gid<group_count; gid++){
        if (verbose) printf("[Client %d] setting up to connect to g/n: %d/%d\n", client->client_id, gid, nid);
        struct connection* conn;
        conn = malloc(sizeof(struct connection*));
        conn->client = client;
        conn->group_id = gid;
        conn->node_id = nid;
        client->mcast_clients[gid] = mcast_client_connect(gid, nid, config, client->base, on_connect_to_host, on_read, conn);
        pthread_mutex_init(&client->lock[gid], NULL);
    }
}
static struct client*
make_client(int client_id)
{
  struct client* c;

  evthread_use_pthreads();
  c = malloc(sizeof(struct client));
  c->mcast_clients_count = 0;
  c->client_id = client_id;
  c->v.client_id = c->client_id;
  c->base = event_base_new();

  return c;
}

static void
client_free(struct client* c)
{
  for (int i = 0; i< c->mcast_clients_count; i++) {
    mcast_client_free(c->mcast_clients[i]);
  }
  event_base_free(c->base);
  free(c);
}

static void *
start_client(void *arg) {
    client_id = *((int *) arg);

    if (verbose) printf("C [Client %d]: starting...\n", client_id);
    client = make_client(client_id);
    connect_to_all_group();
    if (client) {
        event_base_dispatch(client->base);
        client_free(client);
    }
    pthread_exit(NULL);
}


JNIEXPORT void JNICALL
Java_ch_usi_dslab_lel_jmcast_jni_McastClientJNI_jni_1start(JNIEnv *env, jobject obj) {
//    start_client(&client_id);
    pthread_t t;
    /* Create worker thread */
    pthread_create(&t, NULL, start_client, &client_id);
//    pthread_join(t, NULL);
}


JNIEXPORT void JNICALL
Java_ch_usi_dslab_lel_jmcast_jni_McastClientJNI_jni_1init(JNIEnv *env, jobject obj, jint clientId, jstring l_config) {
    client_id = (int) clientId;

    const char *tmp1 = (*env)->GetStringUTFChars(env, l_config, 0);
    config = (char *) tmp1;

    struct evmcast_config *conf= evmcast_config_read(config);
    group_count = evmcast_group_count(conf);

    // convert local to global reference
    // (local will die after this method call)
    g_obj = (*env)->NewGlobalRef(env, obj);

    // save refs for callback
    jclass g_clazz = (*env)->GetObjectClass(env, g_obj);
    if (g_clazz == NULL) {
        printf("Failed to find class");
    }

    g_cb_on_reply = (*env)->GetMethodID(env, g_clazz, "onReply", "([B)V");
    if (g_cb_on_reply == NULL) {
        printf("Unable to get method onDeliver");
    }

    g_cb_on_connected = (*env)->GetMethodID(env, g_clazz, "onConnected", "(Ljava/lang/String;II)V");
    if (g_cb_on_connected == NULL) {
        printf("Unable to get method onConnected");
    }

    if (verbose) printf("C registered callback...\n");

//    (*env)->ReleaseStringUTFChars(env, l_config, tmp1);

    setbuf(stdout, NULL);
}

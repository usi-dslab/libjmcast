/* * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <evamcast.h>
#include <evfamcast.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jni.h>
#include<pthread.h>

#include <event2/thread.h>
#include "uthash/uthash.h"
#define STATS_INTERVAL 5
#define VSIZE 32768

enum server_type_t
{
  MCAST,
  AMCAST,
  FAMCAST
};

struct stats
{
  long delivered;
  long min_latency;
  long max_latency;
  long avg_latency;
};

struct stats_ctrl
{
  int            value_size;
  struct stats   stats;
  struct event*  stats_ev;
  struct timeval stats_interval;
  struct timeval last_tv;
};

struct client_value
{
  struct timeval t;
  short          global;
  size_t         size;
  unsigned long  key;
  int            client_id;
  char           value[VSIZE];

};


static int verbose = 0;

// cached refs for later callbacks
static JavaVM *g_jvm;
static jobject g_obj;
static jmethodID g_cb_on_deliver;

static int group_id, node_id;
static char *mconfig, *pconfig;

struct client_map {
    unsigned client_id;                    /* key */
    struct bufferevent *bev;
    UT_hash_handle hh;         /* makes this structure hashable */
    pthread_mutex_t lock;
};

struct client_map *clients = NULL;

void
store_client(int client_id, struct bufferevent *client) {
    struct client_map *s = malloc(sizeof(struct client_map));;
    HASH_FIND_INT(clients, &client_id, s);  /* id already in the hash? */
    if (s == NULL) {
        s = (struct client_map *) malloc(sizeof(struct client_map));
        s->client_id = client_id;
        pthread_mutex_init(&s->lock, NULL);
        HASH_ADD_INT(clients, client_id, s);  /* id: name of key field */
    }
    s->bev = client;
}

struct client_map *
get_client(int client_id) {
    struct client_map *s;
    HASH_FIND_INT(clients, &client_id, s);  /* s: output pointer */
    return s;
}


jint
JNI_OnLoad(JavaVM *vm, void *reserved) {
    g_jvm = vm;
    JNIEnv *env;
    if ((*vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        printf("NATIVE jni failed");
        return -1;
    }
    return JNI_VERSION_1_8;
}


JNIEXPORT void JNICALL
Java_ch_usi_dslab_lel_jmcast_jni_McastNodeJNI_jni_1init(JNIEnv *env, jobject obj, jint groupId, jint nodeId,
                                                          jstring mConfig, jstring pConfig) {
    group_id = (int) groupId;
    node_id = (int) nodeId;
    const char *tmp1 = (*env)->GetStringUTFChars(env, mConfig, 0);
    mconfig = (char *) tmp1;
    const char *tmp2 = (*env)->GetStringUTFChars(env, pConfig, 0);
    pconfig = (char *) tmp2;

    // convert local to global reference
    // (local will die after this method call)
    g_obj = (*env)->NewGlobalRef(env, obj);

    // save refs for callback
    jclass g_clazz = (*env)->GetObjectClass(env, g_obj);
    if (g_clazz == NULL) {
        printf("Failed to find class");
    }

    g_cb_on_deliver = (*env)->GetMethodID(env, g_clazz, "deliver", "(I[B)V");
    if (g_cb_on_deliver == NULL) {
        printf("Unable to get method ref");
    }

    if (verbose) printf("C registered callback %d %d %s %s...\n", group_id, node_id, mconfig, pconfig);

    //disable buffer for immediately print
    setbuf(stdout, NULL);
}

void trigger_callback(jmethodID callback, int client_id, char *s, int len) {
    JNIEnv *env;

    // double check it's all ok
    int getEnvStat = (*g_jvm)->GetEnv(g_jvm, (void **) &env, JNI_VERSION_1_8);
    if (getEnvStat == JNI_EDETACHED) {
        int ret = (*g_jvm)->AttachCurrentThread(g_jvm, (void **) &env, NULL);
        if (ret!=0) {
            printf("Failed to attach. Error: %d\n", ret);
        }
    } else if (getEnvStat == JNI_EVERSION) {
        printf("GetEnv: version not supported\n");
    }
//     else if (getEnvStat == JNI_OK) {
//        printf("GetEnv: OK\n");
//    }

    jbyteArray js = (*env)->NewByteArray(env, len);
    (*env)->SetByteArrayRegion(env, js, 0, len, s);
    (*env)->CallVoidMethod(env, g_obj, callback, client_id, js);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }

    (*env)->DeleteLocalRef(env, js);
    (*g_jvm)->DetachCurrentThread(g_jvm);
}


JNIEXPORT void JNICALL
Java_ch_usi_dslab_lel_jmcast_jni_McastNodeJNI_jni_1reply_1to_1client(JNIEnv *env, jobject obj, jint client_id,
                                                                       jbyteArray message) {
    struct client_map *client = get_client(client_id);

    mcast_message m;
    memset(&m, 0, sizeof(mcast_message));

    m.value.mcast_value_val = (char*)((*env)->GetByteArrayElements(env, message, 0));
        jsize len = (*env)->GetArrayLength(env, message);

    m.value.mcast_value_len = len;
    m.type = MCAST_CLIENT;
    m.from_group = group_id;
    m.from_node = node_id;
    if (verbose) printf("[%d/%d] start reply to client %d size %d value %s\n", group_id,node_id, client_id, m.value.mcast_value_len, m.value.mcast_value_val);
    if (client && client->bev != NULL) {
        pthread_mutex_lock(&client->lock);
        send_mcast_message(client->bev, &m);
        pthread_mutex_unlock(&client->lock);
    }

    (*env)->ReleaseByteArrayElements(env, message, m.value.mcast_value_val, 0);
    if (verbose) printf("[%d/%d] end reply to client %d size %d value %s\n", group_id,node_id, client_id, m.value.mcast_value_len, m.value.mcast_value_val);
}

JNIEXPORT jint JNICALL
Java_ch_usi_dslab_lel_jmcast_jni_McastNodeJNI_jni_1is_1connected_1to_1client(JNIEnv *env, jobject obj, jint client_id) {
    struct client_map *client = get_client(client_id);
    if (client && client->bev != NULL) {
        return 1;
    }
    return 0;
}


static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d, exiting...\n", sig);
  event_base_loopexit(base, NULL);
}

// Returns t2 - t1 in microseconds.
static long
timeval_diff(struct timeval* t1, struct timeval* t2)
{
  long us;
  us = (t2->tv_sec - t1->tv_sec) * 1e6;
  if (us < 0)
    return 0;
  us += (t2->tv_usec - t1->tv_usec);
  return us;
}

static void
update_stats(struct stats* stats, struct timeval last)
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  long lat = timeval_diff(&last, &tv);

  stats->delivered++;
  stats->avg_latency = stats->avg_latency + ((lat - stats->avg_latency) / stats->delivered);
  if (stats->min_latency == 0 || (lat > 0 && lat < stats->min_latency))
    stats->min_latency = lat;
  if (lat > stats->max_latency)
    stats->max_latency = lat;
}

static void
on_stats(evutil_socket_t fd, short event, void* arg)
{
  struct stats_ctrl* sc = arg;
  struct stats*      s = &(sc->stats);
  int                delta_t = STATS_INTERVAL;
  double             mbps = (double)(s->delivered * sc->value_size * 8) / (1024 * 1024);

  mbps /= delta_t;
//  if (verbose)
  fprintf(stderr, "%ld msgs/sec, %.2f Mbps, message size %d, latency min %ld us max %ld us avg %ld us\n",
          s->delivered / delta_t, mbps, sc->value_size, s->min_latency, s->max_latency, s->avg_latency);
  memset(s, 0, sizeof(struct stats));
  event_add(sc->stats_ev, &sc->stats_interval);
}

static void
deliver(struct bufferevent* client, mcast_message* m, void* arg)
{
    struct stats_ctrl* sc = arg;
    struct mcast_value val = m->value;

    struct client_value c_val;
    memcpy(&c_val, m->value.mcast_value_val, m->value.mcast_value_len);

    if (client!=NULL){
        if (verbose) printf("[%d/%d] DOES Store client %d with bev %d\n", group_id, node_id, c_val.client_id, (client==NULL)?0:1);
        store_client(c_val.client_id, client);
    } else {
        if (verbose) printf("[%d/%d] NOT Store client %d with bev %d\n", group_id, node_id, c_val.client_id, (client==NULL)?0:1);
    }
    if (verbose)
        printf("[%d/%d] Delivering message from client (%d) and %d bytes value %p\n",group_id, node_id,c_val.client_id, val.mcast_value_len, c_val.value);

    sc->value_size = val.mcast_value_len;
    update_stats(&(sc->stats), sc->last_tv);
    gettimeofday(&(sc->last_tv), NULL);

    trigger_callback(g_cb_on_deliver, c_val.client_id, c_val.value, c_val.size);
}

static void
dispatch(struct event_base* base, struct stats_ctrl* sc)
{
  struct event* sig;

  // statistics
  memset(sc, 0, sizeof(struct stats_ctrl));
  sc->stats_interval = (struct timeval){ STATS_INTERVAL, 0 };
  sc->stats_ev = evtimer_new(base, on_stats, sc);
  gettimeofday(&(sc->last_tv), NULL);
  event_add(sc->stats_ev, &sc->stats_interval);

  // signal handling
  sig = evsignal_new(base, SIGINT, handle_sigint, base);
  evsignal_add(sig, NULL);
  signal(SIGPIPE, SIG_IGN);
  event_base_dispatch(base);
  event_free(sc->stats_ev);
  event_free(sig);
}

static void
start_node_amcast(int group_id, int id, const char* config, const char* paxos_config)
{
  struct event_base*     base;
  struct evamcast_node*  n;
  struct stats_ctrl      sc;
  mcast_deliver_function cb = deliver;
  evthread_use_pthreads();
  base = event_base_new();
  n = evamcast_node_init(group_id, id, config, paxos_config, base, cb, &sc, NULL);
  if (n == NULL) {
    printf("Could not start the node.\n");
  } else {
    dispatch(base, &sc);
    evamcast_node_free(n);
  }

  event_base_free(base);
}

static void
start_node_famcast(int group_id, int id, const char* config, const char* paxos_config)
{
  struct event_base*     base;
  struct evfamcast_node*  n;
  struct stats_ctrl      sc;
  mcast_deliver_function cb = deliver;
  evthread_use_pthreads();
  base = event_base_new();
  n = evfamcast_node_init(group_id, id, config, paxos_config, base, cb, &sc);
  if (n == NULL) {
    printf("Could not start the node.\n");
  } else {
    dispatch(base, &sc);
    evfamcast_node_free(n);
  }

  event_base_free(base);
}

JNIEXPORT void JNICALL
Java_ch_usi_dslab_lel_jmcast_jni_McastNodeJNI_jni_1start(JNIEnv *env, jobject obj) {
    printf("Node %d group %d starting %s %s...\n", group_id, node_id, mconfig, pconfig);
    start_node_amcast(group_id, node_id, mconfig, pconfig);
}

#!/usr/bin/env bash

PAXOS_CONF0=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_paxos.conf.0
PAXOS_CONF1=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_paxos.conf.1
PAXOS_CONF2=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_paxos.conf.2
MCAST_CONF=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf

CLIENT_COUNT=50
DESTINATION_COUNT=3
TIME=30
LOG_DIR=~/tmp/c_
LOG_DIR+=$CLIENT_COUNT
LOG_DIR+=_p_
LOG_DIR+=$DESTINATION_COUNT
GLOBAL=0

JAVA_CMD="java -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libjmcast/target/classes/log4j.xml -cp "
CLASSPATH="/home/long/apps/ScalableSMR/libjmcast/../dependencies/*:/home/long/apps/ScalableSMR/libjmcast/../sense/target/classes:/home/long/apps/ScalableSMR/libjmcast/../libmcad/target/classes:/home/long/apps/ScalableSMR/libjmcast/../netwrapper/target/classes:/home/long/apps/ScalableSMR/libjmcast/../sense/target/classes:/home/long/apps/ScalableSMR/libjmcast/./target/classes"

ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 0 $PAXOS_CONF0 > /tmp/px00.log &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 1 $PAXOS_CONF0 > /tmp/px01.log &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 2 $PAXOS_CONF0 > /tmp/px02.log &


ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 0 $PAXOS_CONF1 > /tmp/px10.log &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 1 $PAXOS_CONF1 > /tmp/px11.log &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 2 $PAXOS_CONF1 > /tmp/px12.log &


ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 0 $PAXOS_CONF2 > /tmp/px20.log &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 1 $PAXOS_CONF2 > /tmp/px21.log &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 2 $PAXOS_CONF2 > /tmp/px22.log &

ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/node-simple -g 0 -n 0 -c $MCAST_CONF -p $PAXOS_CONF0 -s amcast &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/node-simple -g 1 -n 0 -c $MCAST_CONF -p $PAXOS_CONF1 -s amcast &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/node-simple -g 2 -n 0 -c $MCAST_CONF -p $PAXOS_CONF2 -s amcast &

# sleep 3
#/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/client-simple -c /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf -a 1
for ((i=1; i<=$CLIENT_COUNT; i++)); do
  let b=$i+5
  #  LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $i /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf 1 node1 40000 $TIME $LOG_DIR &
  ssh -o StrictHostKeyChecking=no 192.168.3.$b /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/client-simple -c $MCAST_CONF -a $GLOBAL &
  ssh -o StrictHostKeyChecking=no 192.168.3.$b /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/client-simple -c $MCAST_CONF -a $GLOBAL &
  ssh -o StrictHostKeyChecking=no 192.168.3.$b /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/client-simple -c $MCAST_CONF -a $GLOBAL &
  ssh -o StrictHostKeyChecking=no 192.168.3.$b /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/client-simple -c $MCAST_CONF -a $GLOBAL &
  ssh -o StrictHostKeyChecking=no 192.168.3.$b /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/client-simple -c $MCAST_CONF -a $GLOBAL &
done

#!/usr/bin/env bash

PAXOS_CONF0=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_paxos.conf.0
PAXOS_CONF1=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_paxos.conf.1
PAXOS_CONF2=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_paxos.conf.2
MCAST_CONF=/home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf

CLIENT_COUNT=50
DESTINATION_COUNT=3
TIME=30
LOG_DIR=~/tmp/c_
LOG_DIR+=$CLIENT_COUNT
LOG_DIR+=_p_
LOG_DIR+=$DESTINATION_COUNT

JAVA_CMD="java -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libjmcast/target/classes/log4j.xml -cp "
CLASSPATH="/home/long/apps/ScalableSMR/libjmcast/../dependencies/*:/home/long/apps/ScalableSMR/libjmcast/../sense/target/classes:/home/long/apps/ScalableSMR/libjmcast/../libmcad/target/classes:/home/long/apps/ScalableSMR/libjmcast/../netwrapper/target/classes:/home/long/apps/ScalableSMR/libjmcast/../sense/target/classes:/home/long/apps/ScalableSMR/libjmcast/./target/classes"

ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 0 $PAXOS_CONF0 > /tmp/px00.log &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 1 $PAXOS_CONF0 > /tmp/px01.log &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 2 $PAXOS_CONF0 > /tmp/px02.log &


ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 0 $PAXOS_CONF1 > /tmp/px10.log &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 1 $PAXOS_CONF1 > /tmp/px11.log &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 2 $PAXOS_CONF1 > /tmp/px12.log &

ssh -o StrictHostKeyChecking=no 192.168.3.2 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 0 $PAXOS_CONF2 > /tmp/px20.log &
ssh -o StrictHostKeyChecking=no 192.168.3.3 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 1 $PAXOS_CONF2 > /tmp/px21.log &
ssh -o StrictHostKeyChecking=no 192.168.3.4 /home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/bin/proposer-acceptor 2 $PAXOS_CONF2 > /tmp/px22.log &

#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.sense.DataGatherer 40000 $LOG_DIR throughput jmcast_learner 3 throughput jmcast_client $CLIENT_COUNT latency jmcast_client $CLIENT_COUNT &
#only collect tp at learner
$JAVA_CMD "$CLASSPATH" ch.usi.dslab.bezerra.sense.DataGatherer 40000 $LOG_DIR throughput jmcast_learner 3 &

ssh -o StrictHostKeyChecking=no 192.168.3.2 LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 0 0 $MCAST_CONF $PAXOS_CONF0 node1 40000 $TIME $LOG_DIR &
#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 0 1 $MCAST_CONF $PAXOS_CONF0 node1 40000 $TIME $LOG_DIR &
#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 0 2 $MCAST_CONF $PAXOS_CONF0 node1 40000 $TIME $LOG_DIR &

ssh -o StrictHostKeyChecking=no 192.168.3.3 LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 1 0 $MCAST_CONF $PAXOS_CONF1 node1 40000 $TIME $LOG_DIR &
#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 1 1 $MCAST_CONF $PAXOS_CONF1 node1 40000 $TIME $LOG_DIR &
#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 1 2 $MCAST_CONF $PAXOS_CONF1 node1 40000 $TIME $LOG_DIR &

ssh -o StrictHostKeyChecking=no 192.168.3.4 LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 2 0 $MCAST_CONF $PAXOS_CONF2 node1 40000 $TIME $LOG_DIR &
#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 2 1 $MCAST_CONF $PAXOS_CONF2 node1 40000 $TIME $LOG_DIR &
#$JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.Server 2 2 $MCAST_CONF $PAXOS_CONF2 node1 40000 $TIME $LOG_DIR &

sleep 3

#java -Xcheck:jni -Dlog4j.configuration=file:target/classes/log4j.xml -classpath "../dependencies/*:../sense/target/classes:./target/classes" BenchClient 1 /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/mcast-2g3p.conf 1 node1 40000 $TIME $LOG_DIR
#java -Xcheck:jni -cp "target/classes:../netwrapper/target/classes:../libjmcast/target/classes:../dependencies/*" InteractiveClient 1 /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/mcast-2g3p.conf
#java -Xcheck:jni -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libjmcast/target/classes/log4j.xml -classpath "../dependencies/*:./target/classes:../netwrapper/target/classes" ch.usi.dslab.lel.jmcast.sample.InteractiveClient 1 /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf
#java -Xcheck:jni -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libjmcast/target/classes/log4j.xml -classpath "../dependencies/*:./target/classes:../netwrapper/target/classes:../sense/target/classes" ch.usi.dslab.lel.jmcast.sample.BenchClient 1 /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf 1 node1 40000 $TIME $LOG_DIR &
# ssh -o StrictHostKeyChecking=no 192.168.3.1 LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so java -Xcheck:jni -Dlog4j.configuration=file:/home/long/apps/ScalableSMR/libjmcast/target/classes/log4j.xml -classpath "/home/long/apps/ScalableSMR/libjmcast/../dependencies/*:/home/long/apps/ScalableSMR/libjmcast/../sense/target/classes:/home/long/apps/ScalableSMR/libjmcast/../libmcad/target/classes:/home/long/apps/ScalableSMR/libjmcast/../netwrapper/target/classes:/home/long/apps/ScalableSMR/libjmcast/../sense/target/classes:/home/long/apps/ScalableSMR/libjmcast/./target/classes" ch.usi.dslab.lel.jmcast.sample.BenchClient 1 /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf 1 node1 40000 30 ~/tmp/test &
id=0
for ((i=1; i<=$CLIENT_COUNT; i++)); do
  let b=$i+5
  let id=$id+1
  #  LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $i /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf 1 node1 40000 $TIME $LOG_DIR &
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $id /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf $DESTINATION_COUNT & #node1 40000 $TIME $LOG_DIR &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $id /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf $DESTINATION_COUNT & #node1 40000 $TIME $LOG_DIR &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $id /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf $DESTINATION_COUNT & #node1 40000 $TIME $LOG_DIR &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $id /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf $DESTINATION_COUNT & #node1 40000 $TIME $LOG_DIR &
  let id=$id+1
  ssh -o StrictHostKeyChecking=no 192.168.3.$b LD_LIBRARY_PATH=/home/long/.local/lib:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib LD_PRELOAD=/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevamcast.so:/home/long/apps/ScalableSMR/libjmcast/libmcast/build/local/lib/libevmcast.so $JAVA_CMD "$CLASSPATH" ch.usi.dslab.lel.jmcast.sample.BenchClient $id /home/long/apps/ScalableSMR/libjmcast/bin/generated_config/cluster/generated_mcast.conf $DESTINATION_COUNT & #node1 40000 $TIME $LOG_DIR &
done
